﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TatamiDoors
{
    public class Bathtub : Object
    {
        private float rotation;

        public Bathtub(int xPos, int yPos, float rotation)
        {
            Position = new Vector2(xPos, yPos);
            boundingRect = new Rectangle(xPos, yPos, 32, 64);
            this.rotation = rotation;
        }

        public override void Update_Physics()
        {
        }

        public override void Update()
        {
        }

        public override void Draw()
        {
            GraphicsDraw.Draw_World("object2",
                                    boundingRect,
                                    new Color(1.0f, 1.0f, 1.0f, 1.0f),
                                    rotation);
        }
    }
}
