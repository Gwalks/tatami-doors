﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TatamiDoors {
    public static class EntityService{
        public static List<Entity> Entities = new List<Entity>();

        public static void Reset(){
            Entities = new List<Entity>();
        }

        //Registers entities
        public static void Register(Entity obj){
            if(!Entities.Contains(obj))
                Entities.Add(obj);
        }
        
        //Removes entities
        public static void Remove(Entity obj){
            while(Entities.Contains(obj))
                Entities.Remove(obj);
        }

        //Updates the entities
        public static void Update(){
            foreach(Entity entity in Entities.ToArray()){
                entity.CalculateVisibility();
                if(entity.IsVisible){
                    entity.Update();
                }
            }
        }

        //Draws the entities
        public static void Draw(){
            foreach(Entity entity in Entities.ToArray())
                if(entity.IsVisible)
                    entity.Draw();
        }
    }
}
