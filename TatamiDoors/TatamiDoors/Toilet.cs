﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TatamiDoors
{
    public class Toilet : Object
    {
        private float glow = 0;
        private float glowAdd = 1.2f;
        private float rotation;
        private Room containingRoom;
        public float fade = 0;

        public Toilet(int xPos, int yPos, float rotation, Room containingRoom)
        {
            boundingRect = new Rectangle(xPos, yPos, 32, 32);
            this.rotation = rotation;
            this.containingRoom = containingRoom;

            if (containingRoom.startRoom)
                fade = 60.0f;
        }

        public override void Update_Physics()
        {
        }

        public override void Update()
        {
            if (glow <= 0)
                glowAdd = 1.2f;
            else if (glow >= 40)
                glowAdd = -1.2f;
            glow += glowAdd;
        }

        public override void Draw()
        {
            if (GameMain.playerOne.checkAlive() &&
                GameMain.playerTwo.checkAlive() &&
                GameMain.usingBathroom[0] &&
                GameMain.usingBathroom[1])
                fade++;
            else if (!GameMain.playerOne.checkAlive() ||
                     !GameMain.playerTwo.checkAlive())
                fade++;
            else
                fade--;

            if (fade > 60f)
                fade = 60f;
            else if (fade < 0)
                fade = 0;
            
            drawGlowParts();

            if (!containingRoom.endRoom)
            {
                if (containingRoom.startRoom && containingRoom.levelExtension <= 10)
                    GraphicsDraw.Draw_World("floor" + containingRoom.levelExtension,
                                            boundingRect,
                                            new Color(1.0f, 1.0f, 1.0f, 1.0f));
                else
                    GraphicsDraw.Draw_World("object1",
                                            boundingRect,
                                            new Color(1.0f, 1.0f, 1.0f, 1.0f),
                                            rotation);
            }
        }

        private void drawGlowParts()
        {
            GraphicsDraw.Draw_World("glowCircle2",
                                    new Rectangle((int)(boundingRect.X - 48 + (16 * (1 - glow / 40.0f))),
                                                  (int)(boundingRect.Y - 48 + (16 * (1 - glow / 40.0f))),
                                                  (int)(128 - (32 * (1 - glow / 40.0f))),
                                                  (int)(128 - (32 * (1 - glow / 40.0f)))),
                                    new Color(fade/60f, fade/60f, fade/60f, fade/60f));

            if ((GameMain.playerOne.CurrentRoom.Equals(containingRoom) || GameMain.playerTwo.CurrentRoom.Equals(containingRoom) &&
                GameMain.endState != 2))
            {
                float angleoffset = (float)Global.Cycle * .075f;
                for (int i = 0; i < 5; i++)
                {
                    float fadeValue = fade / 60.0f * 0.10f;
                    Color r = new Color(fadeValue, fadeValue, fadeValue, fadeValue);
                    GraphicsDraw.Draw_World("GlowTriangle", Global.Vector2FromRectCenter(boundingRect), new Vector2(96, 96), r, (float)Math.PI * 2 / 5 * i + angleoffset);
                    GraphicsDraw.Draw_World("GlowTriangle", Global.Vector2FromRectCenter(boundingRect), new Vector2(96, 96), r, (float)Math.PI * 2 / 5 * (.5f + i) + angleoffset);
                }
            }
        }
    }
}
