﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TatamiDoors
{
    class TrapSpinningTrap : Trap
    {
        private float glow = 0;
        private float glowAdd = 1.2f;

        static readonly Vector2 SIZE = new Vector2(64,64);

        private float rotation;
        public TrapSpinningTrap(Vector2 trapPos){
            Position = trapPos;
            boundingRect = new Rectangle((int)trapPos.X, (int)trapPos.Y, (int)SIZE.X, (int)SIZE.Y);
            rotation = 0;
            Hazardous = true;
        }

        public override void Update_Physics()
        {
        }

        public override void Update()
        {
            if (glow <= 0)
                glowAdd = 1.2f;
            else if (glow >= 40)
                glowAdd = -1.2f;
            glow += glowAdd;

            checkOnHit(GameMain.playerOne);
            checkOnHit(GameMain.playerTwo);
            rotation += .25f;
        }
        public void checkOnHit(Character chara)
        {
            if (new Rectangle(boundingRect.Left+16,
                              boundingRect.Top+16,
                              32,
                              32).Intersects(chara.boundingRect))
            {
                chara.onHit();
            }
        }

        public override void Draw()
        {
            if (GameMain.endState == 0)
            {
                for (int i = 0; i < 12; i++)
                {
                    GraphicsDraw.Draw_World("trap2", boundingRect, new Color(.1f, .1f, .1f, .1f), rotation - .1f * i);
                }

                GraphicsDraw.Draw_World("glowCircle2",
                            new Rectangle((int)(boundingRect.Center.X - 36 + (16 * (1 - glow / 40.0f))),
                                          (int)(boundingRect.Center.Y - 36 + (16 * (1 - glow / 40.0f))),
                                          (int)(72 - (32 * (1 - glow / 40.0f))),
                                          (int)(72 - (32 * (1 - glow / 40.0f)))),
                            Color.Black);
                GraphicsDraw.Draw_World("trap2", boundingRect, Color.White, rotation);
            }
            else
            {
                GraphicsDraw.Draw_World("glowCircle2",
                            new Rectangle((int)(boundingRect.Center.X - 36 + (16 * (1 - glow / 40.0f))),
                                          (int)(boundingRect.Center.Y - 36 + (16 * (1 - glow / 40.0f))),
                                          (int)(72 - (32 * (1 - glow / 40.0f))),
                                          (int)(72 - (32 * (1 - glow / 40.0f)))),
                            Color.Black);
                GraphicsDraw.Draw_World("trap2", boundingRect, Color.White, 0);
            }
        }
    }
}
