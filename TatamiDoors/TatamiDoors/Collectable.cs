﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace TatamiDoors
{
    public class Collectable : Entity
    {
        //The string that leads to the collectable.
        private string textureName;

        //The pickup box.
        Rectangle pickupBox;

        Rectangle roomRect;

        int fade = 60; // 1 second
        float glow = 0;
        float glowAdd = 1.2f;

        int explosionFrame = 1;
        int explosionCounter = 0;

        public Collectable(int xPos, int yPos, Rectangle roomRect)
        {
            Position = new Vector2(xPos, yPos);
            boundingRect = new Rectangle(xPos, yPos,32,32);
            pickupBox = new Rectangle(xPos+10,
                                      yPos+10,
                                      12,
                                      12);
            textureName = "collectable" + Rand.Int(0, 5);
            IsVisible = true;
            this.roomRect = roomRect;
        }

        private void checkPlayer(Character chara)
        {
            if (this.pickupBox.Intersects(chara.boundingRect))
            {
                SoundLoader.Get("PickupCollectible").Play(.4f, 0, 0);
                fade--;
                GameMain.expandFog();
            }
        }

        public override void Update_Physics()
        {
        }

        public override void Update()
        {
            if (glow <= 0)
                glowAdd = 1.2f;
            else if (glow >= 40)
                glowAdd = -1.2f;
            glow += glowAdd;

            if (!IsVisible)
                return;

            if (fade != 60)
            {
                if (--fade == 0)
                    IsVisible = false;
                explosionCounter++;
                if(explosionCounter > 4)
                {
                    explosionFrame++;
                    explosionCounter = 0;
                }
            }
            else if (IsVisible)
            {
                checkPlayer(GameMain.playerOne);
                checkPlayer(GameMain.playerTwo);
            }
        }

        public override void Draw()
        {
            if (roomRect.Contains(GameMain.playerOne.boundingRect.Center)
                || roomRect.Contains(GameMain.playerTwo.boundingRect.Center))
            {
                if (fade == 60)
                {
                    GraphicsDraw.Draw_World("glowCircle2",
                                            new Rectangle((int)(boundingRect.Center.X - 36 + (16 * (1 - glow / 40.0f))),
                                                          (int)(boundingRect.Center.Y - 36 + (16 * (1 - glow / 40.0f))),
                                                          (int)(72 - (32 * (1 - glow / 40.0f))),
                                                          (int)(72 - (32 * (1 - glow / 40.0f)))),
                                            Color.White);
                }

                if (IsVisible)
                {
                    float angleoffset = (float)Global.Cycle * .075f;
                    for (int i = 0; i < 5; i++)
                    {
                        Color r = new Color(.10f * (fade / 60.0f), 
                                            .10f * (fade / 60.0f), 
                                            .10f * (fade / 60.0f), 
                                            .10f * (fade / 60.0f));
                        GraphicsDraw.Draw_World("GlowTriangle", Global.Vector2FromRectCenter(boundingRect), new Vector2(96, 96), r, (float)Math.PI * 2 / 5 * i + angleoffset);
                        GraphicsDraw.Draw_World("GlowTriangle", Global.Vector2FromRectCenter(boundingRect), new Vector2(96, 96), r, (float)Math.PI * 2 / 5 * (.5f + i) + angleoffset);
                    }

                    GraphicsDraw.Draw_World(textureName,
                                            new Rectangle((int)(boundingRect.X + 16 * (1 - fade/60.0f)),
                                                          (int)(boundingRect.Y + 16 * (1 - fade/60.0f)),
                                                          (int)(boundingRect.Width * fade/60.0f),
                                                          (int)(boundingRect.Height * fade/60.0f)),
                                            new Color(fade / 60.0f,
                                                      fade / 60.0f,
                                                      fade / 60.0f,
                                                      fade / 60.0f));
                    if (explosionFrame <= 8 && fade != 60)
                    {
                        GraphicsDraw.Draw_World("explosion" + explosionFrame, boundingRect, Color.White);
                    }
                }
            }
            else if (fade != 60)
            {
                IsVisible = false;
                Remove();
            }
        }
    }
}
