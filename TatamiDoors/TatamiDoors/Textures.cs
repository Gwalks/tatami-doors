﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TatamiDoors{
    public static class Textures{
        public static ContentManager ContentManager;
        static Dictionary<string,Texture2D> ContentList = new Dictionary<string,Texture2D>();

        public static void Initialize(ContentManager manager){
            ContentManager = manager;
        }

        //Clears all assets from memory (they will be reloaded when used again)
        public static void Clear(){
            foreach(Texture2D blah in ContentList.Values)
                blah.Dispose();
            ContentList.Clear();
        }

        //Gets a texture form filepath
        public static Texture2D Get(string name){
            if(!ContentList.ContainsKey(name))
                ContentList.Add(name,ContentManager.Load<Texture2D>(name));
            return ContentList[name];
        }

        //Gets the size of the texture
        public static Vector2 GetSize(string name){
            return GetSize(Get(name));
        }
        public static Vector2 GetSize(Texture2D texture){
            return new Vector2(texture.Width,texture.Height);
        }
    }
}
