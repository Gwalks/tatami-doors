﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace TatamiDoors{
    public static class PlayerInput{
        public static bool IsKeyDown(Keys key){
            return Keyboard.GetState().IsKeyDown(key);
        }
    }
}
