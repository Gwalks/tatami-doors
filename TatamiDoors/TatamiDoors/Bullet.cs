﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TatamiDoors
{
    class Bullet : Entity
    {
        public float rot;
        public string spriteName;
        public Vector4 rectSides;
        public Queue<Rectangle> pastPositions = new Queue<Rectangle>();
        int permanence = 2;

        Rectangle roomRect;

        public Bullet(string sprite, Vector2 pos, Vector2 size, Vector2 velocity, Vector4 rectangleSides, float rotation = 0)
            : base((int)pos.X, (int)pos.Y, (int)size.X, (int)size.Y)
        {
            Velocity = velocity;
            spriteName = sprite;
            rot = rotation;
            rectSides = rectangleSides;
            roomRect = new Rectangle((int)rectSides.X, 
                                     (int)rectSides.Y, 
                                     (int)Math.Abs(rectSides.X - rectSides.Z), 
                                     (int)Math.Abs(rectSides.Y - rectSides.W));
        }

        public void checkOnHit(Character chara)
        {
            if (chara.checkAlive() && chara.boundingRect.Contains(boundingRect.Center))
            {
                chara.onHit();
                Remove();
            }
        }

        private void checkSides()
        {
            if (Velocity.X > 0 && boundingRect.Right >= rectSides.Z)
                Remove();
            if (Velocity.X < 0 && boundingRect.Left <= rectSides.X)
                Remove();
            if (Velocity.Y < 0 && boundingRect.Top <= rectSides.Y)
                Remove();
            if (Velocity.Y > 0 && boundingRect.Bottom >= rectSides.W)
                Remove();
        }

        public override void Update()
        {
            base.Update();

            if (pastPositions.Count >= permanence)
                pastPositions.Dequeue();
            pastPositions.Enqueue(boundingRect);

            checkOnHit(GameMain.playerOne);
            checkOnHit(GameMain.playerTwo);
            checkSides();
        }

        public override void Draw()
        {
            if (roomRect.Contains(GameMain.playerOne.boundingRect.Center) 
                || roomRect.Contains(GameMain.playerTwo.boundingRect.Center))
            {
                for (int i = 0; i < pastPositions.Count - 1; i++)
                {
                    GraphicsDraw.Draw_World(spriteName, 
                                            pastPositions.ElementAt(i), 
                                            new Color((255/permanence)*(i+1),
                                                      (255/permanence)*(i+1),
                                                      (255/permanence)*(i+1),
                                                      (255/permanence)*(i+1)),
                                            rot);
                }
                GraphicsDraw.Draw_World(spriteName, 
                                        boundingRect, 
                                        Color.White, 
                                        rot);
            }
            else
                Remove();
        }

    }
}