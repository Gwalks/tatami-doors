﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace TatamiDoors
{
    public static class Camera
    {
        public static Vector2 Position = Vector2.Zero;
        public static Vector2 positionBefore = Vector2.Zero;
        public static float Scale = 1.0f; //lower scale will means more on the screen at once
        private static float targetScale = 1.0f;
        private static Vector2 targetPosition = Position;
        public static float shake_time = 300;
        public static Stopwatch screenShakeWatch = new Stopwatch();

        private static float CAMERA_MOVE_SPEED = .065f;
        private static float CAMERA_SCALE_SPEED = .065f;

        static bool playerOneAlive;
        static bool playerTwoAlive;
        public static bool destinationReached = true;
        public static bool destScaleReached = true;

        static int shake;

        // Determines which player the Camera is currently tracking
        // 1 for playerOne, 2 for playerTwo, 0 for both players
        public static int playerFocus; 

        public static void Update()
        {
            if(GameMain.endState == 0)
            {
                playerOneAlive = GameMain.playerOne.checkAlive();
                playerTwoAlive = GameMain.playerTwo.checkAlive();

                if (playerOneAlive && playerTwoAlive &&
                    ((!GameMain.playerOne.tracking && !GameMain.playerTwo.tracking) ||
                    (GameMain.playerOne.tracking && GameMain.playerTwo.tracking)))
                {
                    // I made these ints because I believe this helped resolve a camera jitter issue
                    // I'm not 100% sure if this is true, however, because the jitter bug was difficult to reproduce
                    // I'll keep this point in mind for the future
                    // - Max
                    Vector2 playerOneScreenPos = new Vector2((int)GameMain.playerOne.Position.X, (int)GameMain.playerOne.Position.Y);
                    Vector2 playerTwoScreenPos = new Vector2((int)GameMain.playerTwo.Position.X, (int)GameMain.playerTwo.Position.Y);

                    //MidPoint of the two players.
                    int xMid = (int)(playerOneScreenPos.X + playerTwoScreenPos.X) / 2;
                    int yMid = (int)(playerOneScreenPos.Y + playerTwoScreenPos.Y) / 2;

                    //Distance between the two players.
                    float dist = (playerTwoScreenPos - playerOneScreenPos).Length();

                    if (dist * 4 / 3 > (Global.ScreenSize.Y))
                        targetScale = (float)(Global.ScreenSize.Y) / (dist * 4 / 3);

                    positionBefore = Position;
                    targetPosition.X = xMid * targetScale;
                    targetPosition.Y = yMid * targetScale;

                    playerFocus = 0;
                }
                else
                {
                    targetScale = 1.0f;
                    if (!playerOneAlive && !playerTwoAlive)
                    {
                        if (playerFocus == 1)
                        {
                            targetPosition.X = (int)GameMain.playerOne.Position.X;
                            targetPosition.Y = (int)GameMain.playerOne.Position.Y;
                        }
                        else
                        {
                            targetPosition.X = (int)GameMain.playerTwo.Position.X;
                            targetPosition.Y = (int)GameMain.playerTwo.Position.Y;
                        }
                    }
                    else if (!playerOneAlive)
                    {
                        playerFocus = 2;
                        positionBefore = Position;
                        targetPosition.X = (int)GameMain.playerTwo.Position.X;
                        targetPosition.Y = (int)GameMain.playerTwo.Position.Y;
                    }
                    else if (!playerTwoAlive)
                    {
                        playerFocus = 1;
                        positionBefore = Position;
                        targetPosition.X = (int)GameMain.playerOne.Position.X;
                        targetPosition.Y = (int)GameMain.playerOne.Position.Y;
                    }
                    else if (playerOneAlive && GameMain.playerOne.tracking)
                    {
                        playerFocus = 1;
                        positionBefore = Position;
                        targetPosition.X = (int)GameMain.playerOne.Position.X;
                        targetPosition.Y = (int)GameMain.playerOne.Position.Y;
                    }
                    else if (playerTwoAlive && GameMain.playerTwo.tracking)
                    {
                        playerFocus = 2;
                        positionBefore = Position;
                        targetPosition.X = (int)GameMain.playerTwo.Position.X;
                        targetPosition.Y = (int)GameMain.playerTwo.Position.Y;
                    }
                }
            }

            Scale += (targetScale - Scale) * CAMERA_SCALE_SPEED;

            Vector2 diff = Vector2.Subtract(targetPosition, Position);

            float distance = diff.Length();
            if (distance != 0f)
            {
                diff.Normalize();

                if (GameMain.endState == 2)
                {
                    if (distance - CAMERA_MOVE_SPEED < CAMERA_MOVE_SPEED)
                    {
                        destinationReached = true;
                        Position = targetPosition;
                    }
                    else
                    {
                        destinationReached = false;
                        Position += diff * CAMERA_MOVE_SPEED;
                    }
                }
                else
                {
                    if (distance <= CAMERA_MOVE_SPEED)
                        destinationReached = true;
                    else
                        destinationReached = false;
                    Position += diff * distance * CAMERA_MOVE_SPEED;
                }
            }

            if (screenShakeWatch.IsRunning)
            {
                Position.X += Rand.Int(-shake, shake);
                Position.Y += Rand.Int(-shake, shake);

                if (screenShakeWatch.ElapsedMilliseconds >= shake_time)
                {
                    screenShakeWatch.Stop();
                    screenShakeWatch.Reset();
                }
            }
        }

        public static void screenShake(int millisecondsTimer, int shakeAmount)
        {
            shake_time = millisecondsTimer;
            shake = shakeAmount;

            screenShakeWatch.Start();
        }

        public static void reset()
        {
            Scale = 1f;
            targetScale = Scale;
            Position = new Vector2(48.0f, 48.0f);
            targetPosition = new Vector2(48.0f, 48.0f);
            destinationReached = true;
        }

        // Converts a world position to a screen position
        public static Vector2 GetPositionOnScreen(Vector2 worldpos)
        {
            return worldpos * Scale - new Vector2(Position.X, Position.Y) + Global.ScreenSize / 2;
        }

        public static void setTargetPosition(Vector2 newTar)
        {
            targetPosition = newTar;
        }

        public static void setPosition(Vector2 newPos)
        {
            Position = newPos;
        }

        public static void setCameraMoveSpeed(float newSpeed)
        {
            CAMERA_MOVE_SPEED = newSpeed;
        }
    }
}