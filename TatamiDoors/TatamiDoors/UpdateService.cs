﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TatamiDoors {
    public static class UpdateService{
        public static float DeltaTime = 1f/60;//will always be 1/60

        //Updates everything else
        public static void Update(){
            Global.Cycle++;
            //GraphicsDraw.DebugText = "";

            Camera.Update();
            EntityService.Update();
        }
    }
}
