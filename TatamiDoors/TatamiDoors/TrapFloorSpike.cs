﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TatamiDoors
{
    class TrapFloorSpike : Trap
    {
        static readonly Vector2 SIZE = new Vector2(32,32);

        private float glow = 0;
        private float glowAdd = 1.2f;

        public TrapFloorSpike(Vector2 trapPos){
            Position = trapPos;
            boundingRect = new Rectangle((int)trapPos.X, (int)trapPos.Y, (int)SIZE.X, (int)SIZE.Y);
            Hazardous = true;
        }

        public override void Update_Physics()
        {
        }

        public override void Update()
        {
            if (glow <= 0)
                glowAdd = 1.2f;
            else if (glow >= 40)
                glowAdd = -1.2f;
            glow += glowAdd;

            checkOnHit(GameMain.playerOne);
            checkOnHit(GameMain.playerTwo);
        }

        public void checkOnHit(Character chara)
        {
            if(Hazardous && new Rectangle(boundingRect.Left+10,
                                          boundingRect.Top+10,
                                          12,
                                          12).Intersects(chara.boundingRect))
                chara.onHit();
        }

        public override void Draw()
        {
            GraphicsDraw.Draw_World("glowCircle2",
                        new Rectangle((int)(boundingRect.Center.X - 36 + (16 * (1 - glow / 40.0f))),
                                      (int)(boundingRect.Center.Y - 36 + (16 * (1 - glow / 40.0f))),
                                      (int)(72 - (32 * (1 - glow / 40.0f))),
                                      (int)(72 - (32 * (1 - glow / 40.0f)))),
                        Color.Black);

            GraphicsDraw.Draw_World("trap1", boundingRect, Color.White);
        }
    }
}
