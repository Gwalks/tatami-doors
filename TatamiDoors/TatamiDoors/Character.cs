﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace TatamiDoors
{
    enum Direction { Up = 0, Down, Left, Right };

    public class Character : Entity
    {
        const float WALKSPEED = 200;
        const float WALKVELOCITYLERP = .2f;
        const int upAndDownSpriteLength = 4;
        const int leftAndRightSpriteLength = 2;
        const float walkAnimation = 0.1f;
        float walkTimer = 0;
        public int playerNum;
        bool isAlive;
        public bool invincible;
        bool walking = false;
        Direction dir;
        string currentSprite;
        int currentSpriteNum;
        Dictionary<int, String[]> player1Sprites;
        Dictionary<int, String[]> player2Sprites;

        public Room CurrentRoom;
        public Door CurrentDoor;

        public bool loading = false; // For freezing the character for fades between levels

        int fadeValue = 255;
        int fadeIncrement = 10;
        int fadeCounter = 0;

        int explosionFrame = 1;
        int explosionCounter = 0;

        public Queue<Rectangle> pastPositions = new Queue<Rectangle>();
        int permanence = 2;

        public bool tracking = false;

        float glow = 0;
        float glowAdd = 1.2f;

        private float glowFade = 0f;

        public Character(int pNum, int xPos, int yPos)
            : base(xPos, yPos, 32, 32)
        {
            playerNum = pNum;
            isAlive = true;
            Position = new Vector2(xPos, yPos);
            boundingRect = new Rectangle(xPos, yPos, 30, 30);
            addSprites();
            dir = Direction.Down;

            invincible = false;
        }

        private void addSprites()
        {
            if (playerNum == 1)
            {
                player1Sprites = new Dictionary<int, String[]>();
                String[] up = { "player1Back1", "player1Back2", "player1Back3", "player1Back4" };
                String[] down = { "player1Front1", "player1Front2", "player1Front3", "player1Front4" };
                String[] left = { "player1Left1", "player1Left2" };
                String[] right = { "player1Right1", "player1Right2" };
                player1Sprites.Add((int)Direction.Up, up);
                player1Sprites.Add((int)Direction.Down, down);
                player1Sprites.Add((int)Direction.Left, left);
                player1Sprites.Add((int)Direction.Right, right);
                currentSprite = player1Sprites.ElementAt((int)Direction.Down).Value.ElementAt(0);
            }
            if (playerNum == 2)
            {
                player2Sprites = new Dictionary<int, String[]>();
                String[] up = { "player2Back1", "player2Back2", "player2Back3", "player2Back4" };
                String[] down = { "player2Front1", "player2Front2", "player2Front3", "player2Front4" };
                String[] left = { "player2Left1", "player2Left2" };
                String[] right = { "player2Right1", "player2Right2" };
                player2Sprites.Add((int)Direction.Up, up);
                player2Sprites.Add((int)Direction.Down, down);
                player2Sprites.Add((int)Direction.Left, left);
                player2Sprites.Add((int)Direction.Right, right);
                currentSprite = player2Sprites.ElementAt((int)Direction.Down).Value.ElementAt(0);
            }
        }

        private void findNextSprite()
        {
            currentSpriteNum = 0;
            if (playerNum == 1)
            {
                if (dir == Direction.Up)
                    currentSprite = player1Sprites.ElementAt((int)Direction.Up).Value.ElementAt(currentSpriteNum);
                if (dir == Direction.Down)
                    currentSprite = player1Sprites.ElementAt((int)Direction.Down).Value.ElementAt(currentSpriteNum);
                if (dir == Direction.Left)
                    currentSprite = player1Sprites.ElementAt((int)Direction.Left).Value.ElementAt(currentSpriteNum);
                if (dir == Direction.Right)
                    currentSprite = player1Sprites.ElementAt((int)Direction.Right).Value.ElementAt(currentSpriteNum);
            }
            else
            {
                if (dir == Direction.Up)
                    currentSprite = player2Sprites.ElementAt((int)Direction.Up).Value.ElementAt(currentSpriteNum);
                if (dir == Direction.Down)
                    currentSprite = player2Sprites.ElementAt((int)Direction.Down).Value.ElementAt(currentSpriteNum);
                if (dir == Direction.Left)
                    currentSprite = player2Sprites.ElementAt((int)Direction.Left).Value.ElementAt(currentSpriteNum);
                if (dir == Direction.Right)
                    currentSprite = player2Sprites.ElementAt((int)Direction.Right).Value.ElementAt(currentSpriteNum);
            }
        }

        private void updateSprite()
        {
            if (playerNum == 1)
            {
                currentSpriteNum++;
                if (dir == Direction.Up || dir == Direction.Down)
                {
                    if (currentSpriteNum >= upAndDownSpriteLength)
                        currentSpriteNum = 0;
                }
                else
                {
                    if (currentSpriteNum >= leftAndRightSpriteLength)
                        currentSpriteNum = 0;
                }
                currentSprite = player1Sprites.ElementAt((int)dir).Value.ElementAt(currentSpriteNum);
            }
            if (playerNum == 2)
            {
                currentSpriteNum++;
                if (dir == Direction.Up || dir == Direction.Down)
                {
                    if (currentSpriteNum >= upAndDownSpriteLength)
                        currentSpriteNum = 0;
                }
                else
                {
                    if (currentSpriteNum >= leftAndRightSpriteLength)
                        currentSpriteNum = 0;
                }
                currentSprite = player2Sprites.ElementAt((int)dir).Value.ElementAt(currentSpriteNum);
            }
        }

        public bool checkAlive()
        {
            return isAlive;
        }

        public void onHit()
        {
            if (isAlive && !invincible && !loading)
            {
                SoundLoader.Get("deathsound").Play(.4f, 0, 0);
                isAlive = false;
                currentSprite = "death";
                Camera.screenShake(300, 3);
                dieReset();

                if ((playerNum == 1 && (GameMain.playerTwo.CurrentRoom.startRoom || GameMain.playerTwo.CurrentRoom.bathroom)) ||
                    (playerNum == 2 && (GameMain.playerOne.CurrentRoom.startRoom || GameMain.playerOne.CurrentRoom.bathroom)))
                {
                    SoundLoader.Get("PickupCollectible").Play(.4f, 0, 0);
                }
            }
        }

        public bool checkCollision(Rectangle rect)
        {
            return boundingRect.Intersects(rect);
        }

        public void revivePlayer(Character chara)
        {
            if (!chara.isAlive && !this.invincible)
            {
                if (new Rectangle(boundingRect.Left + 10,
                                  boundingRect.Top + 10,
                                  12,
                                  12).Intersects(chara.boundingRect))
                {
                    chara.getRevived();
                    chara.invincible = true;
                }
            }
        }

        public void getRevived()
        {
            SoundLoader.Get("revive").Play(.4f, 0, 0);
            isAlive = true;

            if (playerNum == 1)
                currentSprite = "player1Front1";
            else
                currentSprite = "player2Front2";

            respawnReset();
        }

        private void dieReset()
        {
            explosionFrame = 1;
            explosionCounter = 0;

            fadeValue = 5;
            fadeIncrement = 10;
            fadeCounter = 0;
        }

        private void respawnReset()
        {
            explosionFrame = 8;
            explosionCounter = 0;

            fadeValue = 5;
            fadeIncrement = 10;
            fadeCounter = 0;

            Velocity = Vector2.Zero;
        }

        //Depending on the player the input changes
        //Player 1: WASD
        //Player 2: Arrow Keys
        private void handleInput()
        {
            Vector2 movevector = Vector2.Zero;

            if (playerNum == 1)
            {
                if (PlayerInput.IsKeyDown(Keys.W))
                {
                    if (dir != Direction.Up)
                    {
                        // This check is necessary so that characters won't have excessive
                        // animation switching
                        if (!PlayerInput.IsKeyDown(Keys.A) && !PlayerInput.IsKeyDown(Keys.D))
                        {
                            dir = Direction.Up;
                            findNextSprite();
                        }
                    }
                    movevector += new Vector2(0, -1);
                    tracking = true;
                }
                else if (PlayerInput.IsKeyDown(Keys.S))
                {
                    if (dir != Direction.Down)
                    {
                        if (!PlayerInput.IsKeyDown(Keys.A) && !PlayerInput.IsKeyDown(Keys.D))
                        {
                            dir = Direction.Down;
                            findNextSprite();
                        }
                    }
                    movevector += new Vector2(0, 1);
                    tracking = true;
                }
                if (PlayerInput.IsKeyDown(Keys.A))
                {
                    if (dir != Direction.Left)
                    {
                        dir = Direction.Left;
                        findNextSprite();
                    }
                    movevector += new Vector2(-1, 0);
                    tracking = true;
                }
                else if (PlayerInput.IsKeyDown(Keys.D))
                {
                    if (dir != Direction.Right)
                    {
                        dir = Direction.Right;
                        findNextSprite();
                    }
                    movevector += new Vector2(1, 0);
                    tracking = true;
                }
            }
            else
            {
                if (PlayerInput.IsKeyDown(Keys.Up))
                {
                    if (dir != Direction.Up)
                    {
                        if (!PlayerInput.IsKeyDown(Keys.Left) && !PlayerInput.IsKeyDown(Keys.Right))
                        {
                            dir = Direction.Up;
                            findNextSprite();
                        }
                    }
                    movevector += new Vector2(0, -1);

                    tracking = true;
                }
                else if (PlayerInput.IsKeyDown(Keys.Down))
                {
                    if (dir != Direction.Down)
                    {
                        if (!PlayerInput.IsKeyDown(Keys.Left) && !PlayerInput.IsKeyDown(Keys.Right))
                        {
                            dir = Direction.Down;
                            findNextSprite();
                        }
                    }
                    movevector += new Vector2(0, 1);

                    tracking = true;
                }
                if (PlayerInput.IsKeyDown(Keys.Left))
                {
                    if (dir != Direction.Left)
                    {
                        dir = Direction.Left;
                        findNextSprite();
                    }
                    movevector += new Vector2(-1, 0);

                    tracking = true;
                }
                else if (PlayerInput.IsKeyDown(Keys.Right))
                {
                    if (dir != Direction.Right)
                    {
                        dir = Direction.Right;
                        findNextSprite();
                    }
                    movevector += new Vector2(1, 0);

                    tracking = true;
                }

            }
            if (movevector == Vector2.Zero)
            {
                Velocity = Velocity * (1 - WALKVELOCITYLERP);
                walking = false;
            }
            else
            {
                Velocity = Velocity * (1 - WALKVELOCITYLERP) + Vector2.Normalize(movevector) * WALKSPEED * WALKVELOCITYLERP;
                walking = true;
            }
        }

        public override void Register()
        {
            base.Register();
        }

        public int roomcollisiondisabledcycle_horizontal, roomcollisiondisabledcycle_vertical;
        public void temporarilyDisableRoomCollision_Horizontal()
        {
            roomcollisiondisabledcycle_horizontal = Global.Cycle;
        }
        public void temporarilyDisableRoomCollision_Vertical()
        {
            roomcollisiondisabledcycle_vertical = Global.Cycle;
        }

        public void performCollisions()
        {
            // If the player triggers two different doors, then don't calculate collisions
            if (roomcollisiondisabledcycle_horizontal == Global.Cycle
                && roomcollisiondisabledcycle_vertical == Global.Cycle)
                return;

            // Normal top/bottom wall collisions
            if (roomcollisiondisabledcycle_vertical < Global.Cycle)
            {
                if (this.boundingRect.Top < CurrentRoom.boundingRect.Top)
                {
                    Position.Y = CurrentRoom.boundingRect.Top + boundingRect.Height / 2;
                    boundingRect.Y = (int)Position.Y - boundingRect.Height / 2;

                    // This boolean check is necessary because you can't just simply return out from the loop
                    bool doorCheck = false;
                    foreach (Door d in GameMain.visibleDoors)
                        if (Math.Abs(d.boundingRect.Center.Y - boundingRect.Center.Y) < boundingRect.Height / 2 + 2 &&
                            Math.Abs(d.boundingRect.Center.X - boundingRect.Center.X) < boundingRect.Width / 2 + 2)
                        {
                            doorCheck = true;
                            break;
                        }
                    if(!doorCheck)
                        Velocity.Y = 0;
                }

                else if (this.boundingRect.Bottom > CurrentRoom.boundingRect.Bottom)
                {
                    Position.Y = CurrentRoom.boundingRect.Bottom - boundingRect.Height / 2;
                    boundingRect.Y = (int)Position.Y - boundingRect.Height / 2;
                    bool doorCheck = false;
                    foreach (Door d in GameMain.visibleDoors)
                        if (Math.Abs(d.boundingRect.Center.Y - boundingRect.Center.Y) < boundingRect.Height / 2 + 2 &&
                            Math.Abs(d.boundingRect.Center.X - boundingRect.Center.X) < boundingRect.Width / 2 + 2)
                        {
                            doorCheck = true;
                            break;
                        }
                    if(!doorCheck)
                        Velocity.Y = 0;
                }
            }
            // If calculating vertical (up/down) entry door collision
            else if (CurrentDoor != null)
            {
                if (Math.Abs(boundingRect.Center.Y - CurrentDoor.boundingRect.Center.Y) > boundingRect.Width / 2 - 2)
                {
                    // If you're not near the door, then still calculate wall collisions
                    if (boundingRect.Right > CurrentRoom.boundingRect.Right)
                    {
                        Position.X = CurrentRoom.boundingRect.Right - boundingRect.Width / 2;
                        boundingRect.X = (int)Position.X - boundingRect.Width / 2;
                        Velocity.X = 0;
                    }
                    else if (boundingRect.Left < CurrentRoom.boundingRect.Left)
                    {
                        Position.X = CurrentRoom.boundingRect.Left + boundingRect.Width / 2;
                        boundingRect.X = (int)Position.X - boundingRect.Width / 2;
                        Velocity.X = 0;
                    }
                    return;
                }

                // Otherwise if you are in the door, calculate side collisions
                if (this.boundingRect.Right > CurrentDoor.boundingRect.Right - 2)
                {
                    Position.X = CurrentDoor.boundingRect.Right - 2 - boundingRect.Width / 2;
                    boundingRect.X = (int)Position.X - boundingRect.Width / 2;
                    Velocity.X = 0;
                }

                else if (this.boundingRect.Left < CurrentDoor.boundingRect.Left + 2)
                {
                    Position.X = CurrentDoor.boundingRect.Left + 2 + boundingRect.Width / 2;
                    boundingRect.X = (int)Position.X - boundingRect.Width / 2;
                    Velocity.X = 0;
                }
            }
             
            // Normal left/right wall collisions
            if (roomcollisiondisabledcycle_horizontal < Global.Cycle)
            {
                if (boundingRect.Right > CurrentRoom.boundingRect.Right)
                {
                    Position.X = CurrentRoom.boundingRect.Right - boundingRect.Width / 2;
                    boundingRect.X = (int)Position.X - boundingRect.Width / 2;
                    bool doorCheck = false;
                    foreach (Door d in GameMain.visibleDoors)
                        if (Math.Abs(d.boundingRect.Center.Y - boundingRect.Center.Y) < boundingRect.Height / 2 + 2 &&
                            Math.Abs(d.boundingRect.Center.X - boundingRect.Center.X) < boundingRect.Width / 2 + 2)
                        {
                            doorCheck = true;
                            break;
                        }
                    if (!doorCheck)
                        Velocity.X = 0;
                }

                else if (boundingRect.Left < CurrentRoom.boundingRect.Left)
                {
                    Position.X = CurrentRoom.boundingRect.Left + boundingRect.Width / 2;
                    boundingRect.X = (int)Position.X - boundingRect.Width / 2;
                    boundingRect.Y = (int)Position.Y - boundingRect.Height / 2;
                    bool doorCheck = false;
                    foreach (Door d in GameMain.visibleDoors)
                        if (Math.Abs(d.boundingRect.Center.Y - boundingRect.Center.Y) < boundingRect.Height / 2 + 2 &&
                            Math.Abs(d.boundingRect.Center.X - boundingRect.Center.X) < boundingRect.Width / 2 + 2)
                        {
                            doorCheck = true;
                            break;
                        }
                    if (!doorCheck)
                        Velocity.X = 0;
                }
            }

            // If calculating horizontal (left/right) entry door collision
            else if (CurrentDoor != null)
            {
                if (Math.Abs(boundingRect.Center.X - CurrentDoor.boundingRect.Center.X) > boundingRect.Width / 2 - 2)
                {
                    // If you're not near the door, then still calculate normal wall collisions
                    if (this.boundingRect.Top < CurrentRoom.boundingRect.Top)
                    {
                        Position.Y = CurrentRoom.boundingRect.Top + boundingRect.Height / 2;
                        boundingRect.Y = (int)Position.Y - boundingRect.Height / 2;
                        Velocity.Y = 0;
                    }

                    else if (this.boundingRect.Bottom > CurrentRoom.boundingRect.Bottom)
                    {
                        Position.Y = CurrentRoom.boundingRect.Bottom - boundingRect.Height / 2;
                        boundingRect.Y = (int)Position.Y - boundingRect.Height / 2;
                        Velocity.Y = 0;
                    }
                    return;
                }

                // Otherwise if you are in the door, calculate side collisions
                if (this.boundingRect.Top < CurrentDoor.boundingRect.Top + 2)
                {
                    Position.Y = CurrentDoor.boundingRect.Top + 2 + boundingRect.Height / 2;
                    boundingRect.Y = (int)Position.Y - boundingRect.Height / 2;
                    Velocity.Y = 0;
                }

                else if (this.boundingRect.Bottom > CurrentDoor.boundingRect.Bottom - 2)
                {
                    Position.Y = CurrentDoor.boundingRect.Bottom - 2 - boundingRect.Height / 2;
                    boundingRect.Y = (int)Position.Y - boundingRect.Height / 2;
                    Velocity.Y = 0;
                }
            }
        }

        public override void Update()
        {
            if (!GameMain.playerOne.checkAlive() && (!GameMain.playerTwo.checkAlive()))
                GameMain.LoseGame();

            if (isAlive)
            {
                glowFade--;
                if (glowFade < 0f)
                    glowFade = 0f;

                if (!loading)
                {
                    handleInput();
                    base.Update();
                }

                walkTimer += 1.0f / 60.0f;
                if (playerNum == 1)
                    revivePlayer(GameMain.playerTwo);
                if (playerNum == 2)
                    revivePlayer(GameMain.playerOne);

                if (walking && walkTimer >= walkAnimation)
                {
                    updateSprite();
                    walkTimer = 0;
                }

                if (explosionFrame >= 1)
                {
                    explosionCounter++;
                    if (explosionCounter > 4)
                    {
                        explosionFrame--;
                        explosionCounter = 0;
                    }
                }

                performCollisions();

                if (pastPositions.Count >= permanence)
                    pastPositions.Dequeue();
                pastPositions.Enqueue(boundingRect);
            }

            else
            {
                glowFade++;
                if (glowFade > 60.0f)
                    glowFade = 60.0f;

                if (explosionFrame <= 8)
                {
                    explosionCounter++;
                    if (explosionCounter > 4)
                    {
                        explosionFrame++;
                        explosionCounter = 0;
                    }
                }
            }

            if (glow <= 0)
                glowAdd = 1.2f;
            else if (glow >= 40)
                glowAdd = -1.2f;
            glow += glowAdd;

            fadeCounter++;
            if (fadeCounter > 2)
            {
                if (fadeValue <= 255)
                {
                    fadeValue += fadeIncrement;
                    fadeCounter = 0;
                }
                else
                    invincible = false;
            }
        }

        public override void Draw()
        {
            GraphicsDraw.Draw_World("glowCircle2",
                        new Rectangle((int)(boundingRect.Center.X - 36 + (16 * (1 - glow / 40.0f))),
                                      (int)(boundingRect.Center.Y - 36 + (16 * (1 - glow / 40.0f))),
                                      (int)(72 - (32 * (1 - glow / 40.0f))),
                                      (int)(72 - (32 * (1 - glow / 40.0f)))),
                        new Color(glowFade / 60.0f, glowFade / 60.0f, glowFade / 60.0f, glowFade / 60.0f));

            if (GameMain.playerOne.CurrentRoom.Equals(GameMain.playerTwo.CurrentRoom)
                || (Camera.GetPositionOnScreen(new Vector2(boundingRect.Center.X, boundingRect.Center.Y)).X >= 0 && Camera.GetPositionOnScreen(new Vector2(boundingRect.Center.X, boundingRect.Center.Y)).X <= GraphicsDraw.ScreenX
                && Camera.GetPositionOnScreen(new Vector2(boundingRect.Center.X, boundingRect.Center.Y)).Y >= 0 && Camera.GetPositionOnScreen(new Vector2(boundingRect.Center.X, boundingRect.Center.Y)).Y <= GraphicsDraw.ScreenY))
            {
                float angleoffset = (float)Global.Cycle * .075f;
                for (int i = 0; i < 5; i++)
                {
                    float fadeValue = 0.10f * glowFade / 60.0f;
                    Color r = new Color(fadeValue, fadeValue, fadeValue, fadeValue);
                    GraphicsDraw.Draw_World("GlowTriangle", Global.Vector2FromRectCenter(boundingRect), new Vector2(96, 96), r, (float)Math.PI * 2 / 5 * i + angleoffset);
                    GraphicsDraw.Draw_World("GlowTriangle", Global.Vector2FromRectCenter(boundingRect), new Vector2(96, 96), r, (float)Math.PI * 2 / 5 * (.5f + i) + angleoffset);
                }
            }

            if (isAlive)
            {
                for (int i = 0; i < pastPositions.Count - 1; i++)
                {
                    if (!loading)
                        GraphicsDraw.Draw_World(currentSprite,
                                                pastPositions.ElementAt(i),
                                                new Color((255 / permanence) * (i + 1),
                                                          (255 / permanence) * (i + 1),
                                                          (255 / permanence) * (i + 1),
                                                          (255 / permanence) * (i + 1)));
                }

                GraphicsDraw.Draw_World(currentSprite,
                                        boundingRect,
                                        new Color(255, 255, 255, 
                                        (byte)MathHelper.Clamp(fadeValue, 0, 255)));
                if (explosionFrame >= 1)
                {
                    GraphicsDraw.Draw_World("explosion" + explosionFrame, boundingRect, Color.Black);
                }
            }
            else
            {
                GraphicsDraw.Draw_World(currentSprite, boundingRect, new Color(255, 255, 255, (byte)MathHelper.Clamp(fadeValue, 0, 255)));
                if (explosionFrame <= 8)
                {
                    GraphicsDraw.Draw_World("explosion" + explosionFrame, boundingRect, Color.Black);
                }
            }
        }

    }
}