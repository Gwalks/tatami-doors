﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

using System.Threading;

namespace TatamiDoors
{
    public class GameMain : Game
    {
        //Pointers to the characters.
        public static Character playerOne;
        public static Character playerTwo;
        public static GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        static SoundEffectInstance song;

        public static Pathfinding pathFinding;

        //The state of the end sequence.
        // 0 is not active.
        // 1 is ending screen.
        // 2 is going through levels.
        // 3 is finished.
        public static int endState = 0;
        public static bool onceEnd = false;

        //The level to end on.  12 measn thay after level 11 the end game will play.
        public const int maxLevel = 12;

        public static bool gameLoad; // Variable for tracking transition between levels
        public static bool gameEnd;
        public static int winningPlayer;

        public static int CurrentLevel = 0;
        public static int Winning, Losing;

        //A list of list of rooms.  This does not store all rooms in a level  but
        //stores only the rooms the players have been in.
        public static List<List<Room>> listOfPastLevels;
        public static List<List<Door>> listOfSeenDoorsEver;

        public const int MAX_DEATH_FADE = 3600;
        public static int EndFadeOut = -1;
        public static float deathFade = 0;

        public static int WinningFadeOut = -1, LosingFadeOut = -1;
        public static int EndScreenTime = 260;
        public static int timePerLevelEnd = 475;

        public static int WinningFadeIn = 61, LosingFadeIn = 61;
        public static int StartFadeIn = 181;

        public static bool[] usingBathroom;
        public static List<Door> visibleDoors;

        private static int healTimer = 0; // A timer to track regeneration from collectables

        private static bool gamePause = false;

        public GameMain()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            listOfPastLevels = new List<List<Room>>();
            listOfSeenDoorsEver = new List<List<Door>>();
            listOfPastLevels.Add(new List<Room>());
            usingBathroom = new bool[2];
            visibleDoors = new List<Door>();
            gameLoad = false;
            gameEnd = false;
            pathFinding = null;
        }

        public static void EndGame(int playerNum)
        {
            if (gameEnd) return;
            gameEnd = true;
            Camera.screenShake(3000, 3);
            EndFadeOut = 180;
        }

        public static void WinGame(int playerNum)
        {
            if (CurrentLevel >= maxLevel)
            {
                EndGame(playerNum);
                return;
            }

            if (gameLoad) return;
            gameLoad = true;
            winningPlayer = playerNum;
            WinningFadeOut = 60;
            SoundLoader.Get("Steps2").Play(.4f, 0, 0);
        }

        public static void LoseGame()
        {
            if (gameLoad) return;
            gameLoad = true;
            LosingFadeOut = (int) (deathFade / MAX_DEATH_FADE * 60); // So it doesn't reset
            playerOne.loading = true;
            playerTwo.loading = true;
            SoundLoader.Get("Lose").Play(.4f, 0, 0);
        }

        public static void ResetGameWinning()
        {
            WinningFadeIn = 0;
            
            ResetGame_Shared();
        }

        public static void ResetGameStarting()
        {
            StartFadeIn = 0;
            ResetGame_Shared();
        }

        public static void ResetGameLosing()
        {
            CurrentLevel = 0;
            LosingFadeIn = 0;
            listOfPastLevels.Clear();
            listOfPastLevels.Add(new List<Room>());
            listOfSeenDoorsEver.Clear();

            ResetGame_Shared();
        }

        static void ResetGame_Shared()
        {
            CurrentLevel++;
            listOfSeenDoorsEver.Add(new List<Door>());
            visibleDoors.Clear();
            if (CurrentLevel >= maxLevel && !gameEnd)
            {
                CurrentLevel--;
                EndGame(0);
                return;
            }

            GraphicsDraw.generateNewBackground();
            Camera.reset();
            EntityService.Reset();

            LevelGenerator gen = new LevelGenerator();
            listOfPastLevels.Add(new List<Room>());
            gen.Generate();
            gen.StartLevel();
            playerOne.tracking = false;
            playerTwo.tracking = false;

            deathFade = MAX_DEATH_FADE;

            gameLoad = false;
        }

        //Call to attempt to get the end to start.
        public static void startEndSeq()
        {
            if (!Camera.screenShakeWatch.IsRunning && gameEnd && !onceEnd)
            {
                onceEnd = true;
                endState = 1;
                GraphicsDraw.generateNewBackground();
                startEndGame();
                song.Stop();
                song = null;
                song = SoundLoader.Get("tvsong").CreateInstance();
                song.Volume = 0.4f;
                song.Pitch = 0;
                song.IsLooped = false;
                song.Play();
            }
        }

        public static void startEndGame()
        {
            GraphicsDraw.generateNewBackground();
            Camera.reset();
            EntityService.Reset();
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Textures.Initialize(Content);
            SoundLoader.Initialize(Content);
            GraphicsDraw.Initialize(spriteBatch, this);

            Textures.Get("background0");
            Textures.Get("background1");
            Textures.Get("background2");
            Textures.Get("background3");
            Textures.Get("background4");
            Textures.Get("background5");
            Textures.Get("background6");
            Textures.Get("background7");
            Textures.Get("background8");
            Textures.Get("background9");
            Textures.Get("background10");
            Textures.Get("background11");
            Textures.Get("bullet");
            Textures.Get("collectable0");
            Textures.Get("collectable1");
            Textures.Get("collectable2");
            Textures.Get("collectable3");
            Textures.Get("collectable4");
            Textures.Get("death");
            Textures.Get("door1");
            Textures.Get("door2");
            Textures.Get("door3");
            Textures.Get("door4");
            Textures.Get("door5");
            Textures.Get("door6");
            Textures.Get("door7");
            Textures.Get("end1");
            Textures.Get("end2");
            Textures.Get("end3");
            Textures.Get("end4");
            Textures.Get("end5");
            Textures.Get("end6");
            Textures.Get("end7");
            Textures.Get("end8");
            Textures.Get("end9");
            Textures.Get("explosion1");
            Textures.Get("explosion2");
            Textures.Get("explosion3");
            Textures.Get("explosion4");
            Textures.Get("explosion5");
            Textures.Get("explosion6");
            Textures.Get("explosion7");
            Textures.Get("explosion8");
            Textures.Get("floor1");
            Textures.Get("floor2");
            Textures.Get("floor3");
            Textures.Get("floor4");
            Textures.Get("floor5");
            Textures.Get("floor6");
            Textures.Get("floor7");
            Textures.Get("floor8");
            Textures.Get("floor9");
            Textures.Get("floor10");
            Textures.Get("FloorOverlayA");
            Textures.Get("FloorOverlayB");
            Textures.Get("FloorOverlayC");
            Textures.Get("glowCircle2");
            Textures.Get("GlowTriangle");
            Textures.Get("Mochiguma");
            Textures.Get("object1");
            Textures.Get("object2");
            Textures.Get("player1Back1");
            Textures.Get("player1Back2");
            Textures.Get("player1Back3");
            Textures.Get("player1Back4");
            Textures.Get("player1Front1");
            Textures.Get("player1Front2");
            Textures.Get("player1Front3");
            Textures.Get("player1Front4");
            Textures.Get("player2Left1");
            Textures.Get("player2Left2");
            Textures.Get("player2Right1");
            Textures.Get("player2Right2");
            Textures.Get("player2Back1");
            Textures.Get("player2Back2");
            Textures.Get("player2Back3");
            Textures.Get("player2Back4");
            Textures.Get("player2Front1");
            Textures.Get("player2Front2");
            Textures.Get("player2Front3");
            Textures.Get("player2Front4");
            Textures.Get("player2Left1");
            Textures.Get("player2Left2");
            Textures.Get("player2Right1");
            Textures.Get("player2Right2");
            Textures.Get("Square");
            Textures.Get("texture0");
            Textures.Get("texture1");
            Textures.Get("texture2");
            Textures.Get("texture3");
            Textures.Get("texture4");
            Textures.Get("texture5");
            Textures.Get("texture6");
            Textures.Get("texture7");
            Textures.Get("texture8");
            Textures.Get("texture9");
            Textures.Get("texture10");
            Textures.Get("texture11");
            Textures.Get("title0");
            Textures.Get("title1");
            Textures.Get("title2");
            Textures.Get("title3");
            Textures.Get("title4");
            Textures.Get("title5");
            Textures.Get("title6");
            Textures.Get("title7");
            Textures.Get("trap1");
            Textures.Get("trap2");
            Textures.Get("trap3");

            SoundLoader.Get("deathsound");
            SoundLoader.Get("DoorClose");
            SoundLoader.Get("DoorOpen");
            SoundLoader.Get("Lose");
            SoundLoader.Get("PickupCollectible");
            SoundLoader.Get("revive");
            SoundLoader.Get("Steps2");
            SoundLoader.Get("TurretShoot");
            SoundLoader.Get("tvsong");
            song = SoundLoader.Get("song").CreateInstance();
            song.Volume = 0.4f;
            song.Pitch = 0;
            song.IsLooped = true;
            song.Play();
            ResetGameStarting();
        }

        protected override void OnActivated(object sender, EventArgs args)
        {
            gamePause = false;
            if (song != null)
            {
                song.Resume();
            }
        }

        protected override void  OnDeactivated(object sender, EventArgs args)
        {
            gamePause = true;
            song.Pause();
        }

        protected override void UnloadContent() { }

        protected override void Update(GameTime gameTime)
        {
            if (gamePause)
                return;

            if (PlayerInput.IsKeyDown(Keys.Escape))
                base.Exit();

            startEndSeq();

            if (endState == 0)
            {
                // Reset usingBathrooms for next iteration
                usingBathroom[0] = false;
                usingBathroom[1] = false;

                UpdateService.Update(); //Will update everything else

                base.Update(gameTime);

                if (CurrentLevel >= maxLevel)
                {
                    EndGame(0);
                    return;
                }

                if (EndFadeOut > 0)
                {
                    song.Volume = (EndFadeOut / 180.0f) * 0.4f;
                    EndFadeOut--;
                }
                else if (WinningFadeOut >= 0)
                {
                    if (WinningFadeOut == 0)
                    {
                        WinningFadeOut--;
                        ResetGameWinning();
                    }
                    WinningFadeOut--;
                }
                else if (WinningFadeIn <= 60)
                    WinningFadeIn++;
                else if (LosingFadeOut >= 0)
                {
                    if (LosingFadeOut == 0)
                    {
                        LosingFadeOut--;
                        ResetGameLosing();
                    }
                    LosingFadeOut--;
                }
                else if (LosingFadeIn <= 60)
                    LosingFadeIn++;
                else if (StartFadeIn <= 180)
                    StartFadeIn++;

                // If only one person is alive, check if that person is in a bathroom
                if (!playerOne.checkAlive() || !playerTwo.checkAlive())
                {
                    if (usingBathroom[0] || usingBathroom[1])
                        deathFade += 10.0f;
                    else
                        deathFade--;
                }

                // If both are alive, then check if both are using bathrooms
                // Otherwise -- normally
                else
                {
                    if (usingBathroom[0] && usingBathroom[1])
                        deathFade += 10.0f;
                    else
                        deathFade--;
                }

                if (healTimer > 0)
                {
                    deathFade += 10.0f;
                    healTimer--;
                }

                if (deathFade / MAX_DEATH_FADE <= 0.5f)
                {
                    song.Pitch = (1 - (deathFade / MAX_DEATH_FADE) * 2) * 0.2f;
                }
                else
                    song.Pitch = 0;

                // Check max and min
                if (deathFade >= MAX_DEATH_FADE)
                    deathFade = MAX_DEATH_FADE;
                else if (deathFade <= 0)
                    LoseGame();
            }
            else if (endState == 1)
            {
                //Draw the end screen for a while.
                if (EndScreenTime > 0)
                    EndScreenTime--;

                //If the end screen has been shown for long enough
                //then set the variables up to prepare to walk through them.
                else
                {
                    endState = 2;
                    CurrentLevel = 1;
                    Camera.reset();
                    EntityService.Reset();
                }
            }
            else if (endState == 2)
            {
                if (EndScreenTime > 0)
                    EndScreenTime--;

                Camera.Update();
                //Go through all the levels by drawing them and
                //moving the camera accordingly.
                if (Camera.destinationReached)
                {
                    if (pathFinding != null && pathFinding.path.Count == 0 && EndScreenTime <= 0)
                    {
                        endState = 2;
                        CurrentLevel++;
                        if (CurrentLevel >= listOfPastLevels.Count)
                            endState = 3;
                    }
                    if (pathFinding == null || (pathFinding.path.Count == 0 && endState == 2 && EndScreenTime <= 0))
                    {
                        EndScreenTime = timePerLevelEnd;
                        GraphicsDraw.generateNewBackground();
                        Camera.reset();
                        EntityService.Reset();
                        Room start = null;
                        Room end = null;
                        for (int l = 0; l < listOfPastLevels[CurrentLevel].Count; l++)
                        {
                            if (listOfPastLevels[CurrentLevel][l].startRoom == true)
                            {
                                Camera.setPosition(new Vector2(listOfPastLevels[CurrentLevel][l].boundingRect.Center.X, listOfPastLevels[CurrentLevel][l].boundingRect.Center.Y));
                                start = listOfPastLevels[CurrentLevel][l];
                            }

                            if (listOfPastLevels[CurrentLevel][l].endRoom == true)
                            {
                                end = listOfPastLevels[CurrentLevel][l];
                            }
                        }

                        pathFinding = new Pathfinding(listOfPastLevels[CurrentLevel], start, end);
                        pathFinding.findPath();
                        Camera.setTargetPosition(pathFinding.path[pathFinding.path.Count - 1]);
                        if(CurrentLevel == GameMain.listOfPastLevels.Count - 1)
                            Camera.setCameraMoveSpeed(pathFinding.distance / (timePerLevelEnd - 240));
                        else
                            Camera.setCameraMoveSpeed(pathFinding.distance / (timePerLevelEnd - 120));
                        pathFinding.path.RemoveAt(pathFinding.path.Count - 1);
                    }
                    else if (endState == 2 && pathFinding.path.Count > 0)
                    {
                        Camera.setTargetPosition(pathFinding.path[pathFinding.path.Count-1]);
                        pathFinding.path.RemoveAt(pathFinding.path.Count-1);
                    }
                }   
            }
            else if (endState == 3)
            {
                base.Exit();
            }
        }

        // Picking up a collectable gives +5.0 against the fog each iteration for 1 second
        public static void expandFog()
        {
            healTimer += 60;
        }

        protected override void Draw(GameTime gameTime)
        {
            if (gamePause)
                return;

            float a = .08f;
            GraphicsDevice.Clear(new Color(a, a, a));

            GraphicsDraw.DrawGame();

            base.Draw(gameTime);
        }
    }
}