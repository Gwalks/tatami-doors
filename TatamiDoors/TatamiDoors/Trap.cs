﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TatamiDoors
{
    public abstract class Trap : Entity
    {
        public Vector2 Size;
        public bool Hazardous;
        public Color DrawColor = Color.White;
        public Trap()
        {
        }

        public Boolean isHazardous()
        {
            return Hazardous;
        }
    }
}
