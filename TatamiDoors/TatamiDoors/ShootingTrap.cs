﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace TatamiDoors
{
    class ShootingTrap : Trap
    {
        static readonly Vector2 SIZE = new Vector2(32,32);

        public Bullet bullet;
        public Vector2 bulletVel;
        private Vector4 rectSides;

        public float fireTimer, resetFire;
        public float rot;

        private float glow = 0;
        private float glowAdd = 1.2f;

        static int LastShootCycle;

        public ShootingTrap(Vector2 position, Vector2 bulletVelocity, float shootTimer, Vector4 rectangleSides, float rotation = 0)
        {
            Position = position;
            Size = SIZE;
            boundingRect = new Rectangle((int)(position.X), (int)(position.Y), (int)SIZE.X, (int)SIZE.Y);

            bulletVel = bulletVelocity;
            fireTimer = resetFire = shootTimer;
            rot = rotation;
            Hazardous = true;

            rectSides = rectangleSides;
        }

        public void createBullet()
        {
            if (bulletVel.X > 0 && bulletVel.Y == 0)
                bullet = new Bullet("bullet", new Vector2(boundingRect.X + Size.X / 2, boundingRect.Y + Size.Y / 2),
                    new Vector2(32, 25), bulletVel, rectSides);
            if (bulletVel.X == 0 && bulletVel.Y > 0)
                bullet = new Bullet("bullet", new Vector2(boundingRect.X + Size.X / 2, boundingRect.Y + Size.Y / 2), 
                    new Vector2(32, 25), bulletVel, rectSides, rot);
            if (bulletVel.X < 0 && bulletVel.Y == 0)
                bullet = new Bullet("bullet", new Vector2(boundingRect.X + Size.X / 2, boundingRect.Y + Size.Y / 2),
                    new Vector2(32, 25), bulletVel, rectSides, rot);
            if (bulletVel.X == 0 && bulletVel.Y < 0)
                bullet = new Bullet("bullet", new Vector2(boundingRect.X + Size.X / 2, boundingRect.Y + Size.Y / 2),
                    new Vector2(32, 25), bulletVel, rectSides, rot);
            bullet.Position += bulletVel/10;
            if(Global.Cycle != LastShootCycle){
                LastShootCycle = Global.Cycle;
                SoundLoader.Get("TurretShoot").Play(.4f,0,0);
            }

            bullet.Register();
        }

        public void checkOnHit(Character chara)
        {
            if (Hazardous && new Rectangle(boundingRect.Left + 10, 
                                           boundingRect.Top + 10, 
                                           12, 
                                           12).Intersects(chara.boundingRect))
                chara.onHit();
        }

        public override void Update_Physics()
        {
        }

        public override void Update()
        {
            if (glow <= 0)
                glowAdd = 1.2f;
            else if (glow >= 40)
                glowAdd = -1.2f;
            glow += glowAdd;

            if (fireTimer <= 0)
            {
                createBullet();
                fireTimer = resetFire;
            }
            fireTimer--;

            checkOnHit(GameMain.playerOne);
            checkOnHit(GameMain.playerTwo);
        }

        public override void Draw()
        {
            GraphicsDraw.Draw_World("glowCircle2",
                        new Rectangle((int)(boundingRect.Center.X - 36 + (16 * (1 - glow / 40.0f))),
                                      (int)(boundingRect.Center.Y - 36 + (16 * (1 - glow / 40.0f))),
                                      (int)(72 - (32 * (1 - glow / 40.0f))),
                                      (int)(72 - (32 * (1 - glow / 40.0f)))),
                        Color.Black);

            GraphicsDraw.Draw_World("trap3", boundingRect, Color.White, rot);
        }
    }
}
