﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TatamiDoors {
    public static class Global{
        public static int Cycle;//Incremented by 1 every update

        public const bool MAPHAX = false;//false;

        public static Vector2 ScreenSize;

        public static Vector2 TileSize = new Vector2(32, 32);

        public static Vector2 Vector2FromRectCenter(Rectangle rect){
            return new Vector2(rect.Center.X,rect.Center.Y);
        }
    }
}
