﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TatamiDoors {
    public abstract class Entity
    {
        public Vector2 Position;
        public Vector2 Velocity;
        public bool IsVisible;
        public Rectangle boundingRect;

        public Entity() {}

        public Entity(int posX, int posY, int w, int h)
        {
            Position = new Vector2(posX, posY);
            boundingRect = new Rectangle(posX, posY, w, h);
        }

        //Registers the entity with EntityService; must be called after you create the entity
        public virtual void Register(){
            EntityService.Register(this);
        }

        //Removes the entity
        public virtual void Remove()
        {
            EntityService.Remove(this);
        }

        //This should check if the entity will be drawn and updated (if it's in a room that's visible)
        public virtual void CalculateVisibility()
        {
            IsVisible = true;//always will for now (until we have rooms and stuff)
        }

        //This should update all physics stuff; override it with an empty function for stuff like stationary traps
        public virtual void Update_Physics()
        {
            Position += Velocity*UpdateService.DeltaTime;
            boundingRect.X = (int)Position.X-boundingRect.Width/2;
            boundingRect.Y = (int)Position.Y-boundingRect.Height/2;
        }

        //The object updates itself
        public virtual void Update()
        {
            Update_Physics();
        }

        //The object should draw itself
        public abstract void Draw();
    }
}
