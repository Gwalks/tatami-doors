﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace TatamiDoors
{
    public static class GraphicsDraw
    {
        public static SpriteBatch spritebatch;

        //static SpriteFont font;

        public static Texture2D rect;

        private static string background;
        private static Color backColor;
        private static Random rand = new Random();

        [DllImport("user32.dll")]
        private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndIntertAfter,
            int X, int Y, int cx, int cy, int uFlags);

        [DllImport("user32.dll")]
        private static extern int GetSystemMetrics(int Which);

        private const int SM_CXSCREEN = 0;
        private const int SM_CYSCREEN = 1;
        private static IntPtr HWND_TOP = IntPtr.Zero;
        private const int SWP_SHOWWINDOW = 64;

        private static bool animateUp = true;
        private static int frame = 0;

        //Save the list of backgrounds for pathing portion.
        struct backgroundPack
        {
            public int backNum;
            public float r;
            public float g;
            public float b;
        }

        private static List<backgroundPack> listOfOldBackgrounds;


        public static int ScreenX
        {
            get { return GetSystemMetrics(SM_CXSCREEN); }
        }
        public static int ScreenY
        {
            get { return GetSystemMetrics(SM_CYSCREEN); }
        }
        public static void Initialize(SpriteBatch sbatch, Game g)
        {
            spritebatch = sbatch;

            Screen screen = Screen.FromHandle(g.Window.Handle);
            screen = screen == null ? Screen.PrimaryScreen : screen;

            Global.ScreenSize = new Vector2(screen.Bounds.Width, screen.Bounds.Height);

            GameMain.graphics.IsFullScreen = true;
            GameMain.graphics.PreferredBackBufferWidth = screen.Bounds.Width;
            GameMain.graphics.PreferredBackBufferHeight = screen.Bounds.Height;
            GameMain.graphics.ApplyChanges();

            g.Window.IsBorderless = true;
            g.Window.SetPosition(Microsoft.Xna.Framework.Point.Zero);
            SetWindowPos(g.Window.Handle, HWND_TOP, 0, 0, ScreenX, ScreenY, SWP_SHOWWINDOW);

            listOfOldBackgrounds = new List<backgroundPack>();
        }

        /////Draw functions
        public static void Draw_Screen(string texturepath, Vector2 position, Vector2 size, Color color, float rotation = 0)
        {
            Vector2 texturesize = Textures.GetSize(texturepath);
            spritebatch.Draw(Textures.Get(texturepath), position, null, color, rotation, texturesize / 2, size / texturesize, SpriteEffects.None, 0);

        }

        public static void Draw_World(string texturepath, Vector2 position, Vector2 size, Color color, float rotation = 0)
        {
            Draw_Screen(texturepath, Camera.GetPositionOnScreen(position), size * Camera.Scale, color, rotation);
        }
        public static void Draw_World(string texturepath, Rectangle boundingrect, Color color, float rotation = 0)
        {
            Draw_World(texturepath, Global.Vector2FromRectCenter(boundingrect), new Vector2(boundingrect.Width, boundingrect.Height), color, rotation);
        }

        public static void Draw_World_FromTopLeft(string texturepath, Rectangle boundingRect, Color color, Rectangle sourceRect)
        {
            Vector2 texturesize = Textures.GetSize(texturepath);
            spritebatch.Draw(Textures.Get(texturepath),
                             Camera.GetPositionOnScreen(new Vector2(boundingRect.Left, boundingRect.Top)),
                             sourceRect, color, 0, Vector2.Zero,
                             Camera.Scale * new Vector2(boundingRect.Width, boundingRect.Height) / texturesize,
                             SpriteEffects.None, 0);
        }

        public static void Draw_World_FromTopLeft_NoScale(string texturepath, Rectangle boundingRect, Color color, Rectangle sourceRect)
        {
            Vector2 texturesize = Textures.GetSize(texturepath);
            spritebatch.Draw(Textures.Get(texturepath),
                             new Vector2((-Camera.Position.X / Camera.Scale) - 5000, (-Camera.Position.Y / Camera.Scale) - 5000),
                             sourceRect, color, 0, Vector2.Zero,
                             new Vector2(boundingRect.Width, boundingRect.Height) / texturesize,
                             SpriteEffects.None, 0);
        }

        /////Draws the game
        public static void DrawGame()
        {

            spritebatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.AnisotropicWrap, DepthStencilState.Default, RasterizerState.CullNone);

            if (GameMain.endState == 1)
            {
                Draw_World_FromTopLeft(background, new Rectangle((int)(-5000 + (Camera.Position.X - Camera.positionBefore.X) * .75), (int)(-5000 + (Camera.Position.Y - Camera.positionBefore.Y) * .75), 96, 96), backColor, new Rectangle(-5000, -5000, (int)5000 * 2, (int)5000 * 2));

                if (GameMain.EndScreenTime < 120)
                {
                    if (animateUp)
                    {
                        if (GameMain.EndScreenTime % 2 == 0)
                        {
                            if (frame == 7)
                                animateUp = false;
                            else if (frame != 7)
                                frame++;
                        }
                    }
                    else
                    {
                        if (GameMain.EndScreenTime % 2 == 0)
                        {
                            if (frame == 0)
                                frame = 0;
                            else if (frame != 0)
                                frame--;
                        }
                    }
                }
                
                if(GameMain.EndScreenTime < 200)
                    Draw_Screen("title" + frame,
                                Global.ScreenSize / 2,
                                new Vector2(688, 206) * (1 - ((1 - GameMain.EndScreenTime/260.0f) * 0.15f)),
                                new Color(1f, 1f, 1f, 1f));
                else
                    Draw_Screen("title" + frame,
                                Global.ScreenSize / 2,
                                new Vector2(688, 206) * (1 - ((1 - GameMain.EndScreenTime / 260.0f) * 0.15f)),
                                new Color(1 - (GameMain.EndScreenTime - 200) / 60.0f,
                                          1 - (GameMain.EndScreenTime - 200) / 60.0f,
                                          1 - (GameMain.EndScreenTime - 200) / 60.0f,
                                          1 - (GameMain.EndScreenTime - 200) / 60.0f));

                Draw_Screen("TatamiFog",
                            Global.ScreenSize / 2,
                            new Vector2(Global.ScreenSize.X * 4,
                                        Global.ScreenSize.X * 4),
                            Color.Black);

                if (GameMain.EndScreenTime >= 200)
                    Draw_Screen("Square",
                                Global.ScreenSize / 2,
                                Global.ScreenSize,
                                new Color(1 - (260 - GameMain.EndScreenTime) / 60.0f,
                                          1 - (260 - GameMain.EndScreenTime) / 60.0f,
                                          1 - (260 - GameMain.EndScreenTime) / 60.0f,
                                          1 - (260 - GameMain.EndScreenTime) / 60.0f));
                else if(GameMain.EndScreenTime <= 60)
                    Draw_Screen("Square",
                                Global.ScreenSize / 2,
                                Global.ScreenSize,
                                new Color(1 - GameMain.EndScreenTime / 60.0f,
                                          1 - GameMain.EndScreenTime / 60.0f,
                                          1 - GameMain.EndScreenTime / 60.0f,
                                          1 - GameMain.EndScreenTime / 60.0f));
            }
            else if (GameMain.endState == 2)
            {
                Draw_World_FromTopLeft(background, new Rectangle((int)(-5000 + (Camera.Position.X - Camera.positionBefore.X) * .75), (int)(-5000 + (Camera.Position.Y - Camera.positionBefore.Y) * .75), 96, 96), backColor, new Rectangle(-5000, -5000, (int)5000 * 2, (int)5000 * 2));

                EntityService.Draw();
                Draw_Screen("TatamiFog",
                            Global.ScreenSize / 2,
                            new Vector2(Global.ScreenSize.X * 4,
                                        Global.ScreenSize.X * 4),
                            Color.Black);

                if (GameMain.EndScreenTime >= 403)
                    Draw_Screen("Square",
                                Global.ScreenSize / 2,
                                Global.ScreenSize,
                                new Color(1 - (475 - GameMain.EndScreenTime) / 60.0f,
                                          1 - (475 - GameMain.EndScreenTime) / 60.0f,
                                          1 - (475 - GameMain.EndScreenTime) / 60.0f,
                                          1 - (475 - GameMain.EndScreenTime) / 60.0f));
                if(GameMain.CurrentLevel == GameMain.listOfPastLevels.Count - 1)
                {
                    if (GameMain.EndScreenTime <= 180)
                    {
                        Draw_Screen("Square",
                                    Global.ScreenSize / 2,
                                    Global.ScreenSize,
                                    new Color(1 - GameMain.EndScreenTime / 180.0f,
                                              1 - GameMain.EndScreenTime / 180.0f,
                                              1 - GameMain.EndScreenTime / 180.0f,
                                              1 - GameMain.EndScreenTime / 180.0f));
                    }
                }
                else
                {
                    if (GameMain.EndScreenTime <= 60)
                        Draw_Screen("Square",
                                    Global.ScreenSize / 2,
                                    Global.ScreenSize,
                                    new Color(1 - GameMain.EndScreenTime / 60.0f,
                                              1 - GameMain.EndScreenTime / 60.0f,
                                              1 - GameMain.EndScreenTime / 60.0f,
                                              1 - GameMain.EndScreenTime / 60.0f));
                }
            }
            else
            {
                Draw_World_FromTopLeft_NoScale(background, 
                                               new Rectangle((int)(-5000 + (Camera.Position.X - Camera.positionBefore.X) * .75), 
                                                             (int)(-5000 + (Camera.Position.Y - Camera.positionBefore.Y) * .75), 
                                                             96, 
                                                             96), 
                                               backColor, 
                                               new Rectangle(-5000, 
                                                             -5000, 
                                                             (int)5000 * 2, 
                                                             (int)5000 * 2));
                EntityService.Draw();

                drawFog();

                int fadeOut = GameMain.EndFadeOut;
                if (fadeOut >= 0)
                {
                    Draw_Screen("Square",
                                Vector2.Zero,
                                Global.ScreenSize * 2,
                                new Color(1 - fadeOut / 180.0f,
                                          1 - fadeOut / 180.0f,
                                          1 - fadeOut / 180.0f,
                                          1 - fadeOut / 180.0f));
                }

                fadeOut = GameMain.WinningFadeOut;
                if (fadeOut >= 0)
                    Draw_Screen("Square",
                                Vector2.Zero,
                                Global.ScreenSize * 2,
                                new Color(1 - fadeOut / 60.0f,
                                          1 - fadeOut / 60.0f,
                                          1 - fadeOut / 60.0f,
                                          1 - fadeOut / 60.0f));
                else if (GameMain.LosingFadeOut >= 0)
                    Draw_Screen("Square",
                                Vector2.Zero,
                                Global.ScreenSize * 2,
                                new Color(0, 0, 0, 1 - GameMain.LosingFadeOut / 60.0f));

                int fadeIn = GameMain.StartFadeIn;
                if(fadeIn <= 180)
                    Draw_Screen("Square",
                                Vector2.Zero,
                                Global.ScreenSize * 2,
                                new Color(1 - fadeIn / 180.0f,
                                          1 - fadeIn / 180.0f,
                                          1 - fadeIn / 180.0f,
                                          1 - fadeIn / 180.0f));

                fadeIn = GameMain.WinningFadeIn;
                if (fadeIn <= 60)
                    Draw_Screen("Square",
                                Vector2.Zero,
                                Global.ScreenSize * 2,
                                new Color(1 - fadeIn / 60.0f,
                                          1 - fadeIn / 60.0f,
                                          1 - fadeIn / 60.0f,
                                          1 - fadeIn / 60.0f));
                else if (GameMain.LosingFadeIn <= 60)
                {
                    Draw_Screen("Square",
                                Vector2.Zero,
                                Global.ScreenSize * 2,
                                new Color(0, 0, 0, 1 - GameMain.LosingFadeIn / 60.0f));
                }

                if (fadeOut < 0 && fadeIn > 60)
                {
                    Draw_Screen("Square",
                                Vector2.Zero,
                                Global.ScreenSize * 2,
                                new Color(0, 0, 0, 1 - (float)GameMain.deathFade / GameMain.MAX_DEATH_FADE));
                }
            }
            spritebatch.End();
        }

        private static float stretch; 
        private static void drawFog()
        {
            stretch = (GameMain.deathFade / GameMain.MAX_DEATH_FADE) * 3.0f;

            Draw_Screen("TatamiFog",
                        Global.ScreenSize / 2,
                        new Vector2(Global.ScreenSize.X * (stretch + 1),
                                    Global.ScreenSize.X * (stretch + 1)),
                        Color.Black);
        }

        public static void generateNewBackground()
        {
            int ran = rand.Next(0, 12);
            background = "background" + ran.ToString();
            if (GameMain.endState == 0)
            {
                float rRand = (float)rand.NextDouble();
                float gRand = (float)rand.NextDouble();
                float bRand = (float)rand.NextDouble();
                backColor = new Color(new Vector3(rRand, gRand, bRand));
                backgroundPack bPack = new backgroundPack();
                bPack.backNum = ran;
                bPack.r = rRand;
                bPack.g = gRand;
                bPack.b = bRand;
                listOfOldBackgrounds.Add(bPack);
            }
            else if (GameMain.endState == 2)
            {
                background = "background" + listOfOldBackgrounds[GameMain.CurrentLevel - 1].backNum.ToString();
                backColor = new Color(new Vector3(listOfOldBackgrounds[GameMain.CurrentLevel - 1].r, listOfOldBackgrounds[GameMain.CurrentLevel - 1].g, listOfOldBackgrounds[GameMain.CurrentLevel - 1].b));
            }
            else
            {
                backColor = new Color(128 + Rand.Int(128), 128 + Rand.Int(128), 128 + Rand.Int(128));
            }
        }
    }
}
