﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TatamiDoors{
    public static class Rand{
        public static Random random = new Random();

        //Random integers
        public static int Int(int a,int b){
            return random.Next(a,b);
        }
        public static int Int(int b){
            return random.Next(b);
        }
        
        //Random floats
        public static float Float(){//from 0 to 1
            return (float)random.NextDouble();
        }
        public static float Float(float b){//from 0 to b
            return b*(float)random.NextDouble();
        }
        public static float Float(float a,float b){//from a to b
            return a+(b-a)*(float)random.NextDouble();
        }

        //Angle
        public static float Angle(){
            return Rand.Float()*2*(float)Math.PI;
        }

        //Random chance (for example, a .5 would have a 50% chance of returning true)
        public static bool Chance(float a){
            return Rand.Float() < a;
        }

        //Random color (with a brightness of "totalbrightness" (0 to 1))
        public static Color Color(float totalbrightness){
            float r = Rand.Float(),g = Rand.Float(),b = Rand.Float();
            float max = totalbrightness*3/Math.Max(Math.Max(r,g),b);
            return new Color(r*max,g*max,b*max);
        }
        
        //returns an array of length len with random numbers from 0 to maxnum (no repeats)
        public static int[] Fill(int len,int maxnum){
            int[] ans = new int[len];
            for(int i = 0; i < len; i++){
                int a;
                do{
                    a = Rand.Int(maxnum);
                }while(ans.Contains(a));
                ans[i] = a;
            }
            return ans;
        }
        
        //returns an array of length len with random values from the array "from" (no repeats)
        public static T[] Fill<T>(T[] from,int len){
            if(from.Length < len) throw new ArgumentException();
            T[] ans = new T[len];
            for(int i = 0; i < len; i++){
                T a;
                do{
                    a = from[Rand.Int(from.Length)];
                }while(ans.Contains<T>(a));
                ans[i] = a;
            }
            return ans;
        }
    }
}
