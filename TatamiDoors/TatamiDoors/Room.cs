﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TatamiDoors
{
    public class Room : Entity{
        public int roomNum;
        public Trap[] trapArray;
        public Collectable collectable;
        public Object[] objectArray;
        public List<Character> characterArray;
        public List<Room> surroundingRooms;
        public List<Door> doors;
        public WinToken winToken;
        public int roomWidth;
        public int roomHeight;
        public Color RoomColor;
        public Color TrapColor;
        public Color wallColor;
        public bool bathroom;
        public bool startRoom;
        public bool endRoom;
        int floorOverlayNumber;
        public int levelExtension;
        private int textureNumber;

        private Color overlayColor;

        public Room(int roomNumber, int xPos, int yPos, int width, int height, bool startRoom = false, bool bathroom = false){
            endRoom = false;
            roomNum = roomNumber;
            roomWidth = width;
            roomHeight = height;
            characterArray = new List<Character>();
            surroundingRooms = new List<Room>();
            doors = new List<Door>();
            this.startRoom = startRoom;
            this.bathroom = bathroom;
            boundingRect = new Rectangle(xPos, yPos, (int)(width*Global.TileSize.X) , (int)(height*Global.TileSize.Y));
            levelExtension = 12 - GameMain.CurrentLevel;
            objectArray = new Object[0];
            floorOverlayNumber = Rand.Int(1, 4); // 1, 2, or 3
            trapArray = new Trap[0];
            textureNumber = Rand.Int(12);
            overlayColor = new Color(Rand.Int(56) + 200, Rand.Int(56) + 200, Rand.Int(56) + 200);
            wallColor = new Color(Rand.Int(56), Rand.Int(56), Rand.Int(56));
            if (bathroom)
                RoomColor = new Color(Rand.Int(128), Rand.Int(128), Rand.Int(128));
            else
                RoomColor = new Color(Rand.Int(256), Rand.Int(256), Rand.Int(256));
        }

        public void SetTraps(Trap[] trapList){
            trapArray = trapList;
        }

        public void SetCollectable(Collectable c)
        {
            collectable = c;
        }

        public void SetObjects(Object[] objects)
        {
            objectArray = objects;
        }

        public override void CalculateVisibility(){
            IsVisible = Global.MAPHAX || characterArray.Count != 0;
        }

        public override void Update(){
            for (int k = 0; k < trapArray.Length; k++)
            {
                trapArray[k].Update();
            }

            if (collectable != null)
                collectable.Update();
            
            if (objectArray.Length > 0)
            {
                for (int k = 0; k < objectArray.Length; k++)
                {
                    objectArray[k].Update();
                }

                if ((GameMain.playerOne.CurrentRoom.boundingRect.Intersects(this.boundingRect) &&
                     GameMain.playerOne.checkAlive()))
                {
                    GameMain.usingBathroom[0] = true;
                }

                if((GameMain.playerTwo.CurrentRoom.boundingRect.Intersects(this.boundingRect) &&
                     GameMain.playerTwo.checkAlive()))
                {
                    GameMain.usingBathroom[1] = true;
                }
            }
        }

        const int BORDERSIZE = 8;
        public override void Draw(){
            Vector2 pos = Global.Vector2FromRectCenter(boundingRect);

            GraphicsDraw.Draw_World_FromTopLeft("texture" + textureNumber,
                                                new Rectangle(boundingRect.Left, boundingRect.Top, 96, 96),
                                                RoomColor,
                                                new Rectangle(0, 0, boundingRect.Width, boundingRect.Height));

            if (floorOverlayNumber == 1)
                GraphicsDraw.Draw_World_FromTopLeft("FloorOverlayA",
                                                    new Rectangle(boundingRect.Left, boundingRect.Top, 288, 288),
                                                    overlayColor,
                                                    new Rectangle(0, 0, boundingRect.Width, boundingRect.Height));
            else if (floorOverlayNumber == 2)
                GraphicsDraw.Draw_World_FromTopLeft("FloorOverlayB",
                                                    new Rectangle(boundingRect.Left, boundingRect.Top, 288, 288),
                                                    overlayColor,
                                                    new Rectangle(0, 0, boundingRect.Width, boundingRect.Height));
            else
                GraphicsDraw.Draw_World_FromTopLeft("FloorOverlayC",
                                                    new Rectangle(boundingRect.Left, boundingRect.Top, 288, 288),
                                                    overlayColor,
                                                    new Rectangle(0, 0, boundingRect.Width, boundingRect.Height));

            GraphicsDraw.Draw_World("Square",
                                    new Vector2(pos.X,boundingRect.Top),
                                    new Vector2(boundingRect.Width+BORDERSIZE,BORDERSIZE),
                                    wallColor);
            GraphicsDraw.Draw_World("Square",
                                    new Vector2(pos.X,boundingRect.Bottom),
                                    new Vector2(boundingRect.Width+BORDERSIZE,BORDERSIZE),
                                    wallColor);
            GraphicsDraw.Draw_World("Square",
                                    new Vector2(boundingRect.Left,pos.Y),
                                    new Vector2(BORDERSIZE,
                                    boundingRect.Height+BORDERSIZE),
                                    wallColor);
            GraphicsDraw.Draw_World("Square",
                                    new Vector2(boundingRect.Right,pos.Y),
                                    new Vector2(BORDERSIZE,boundingRect.Height+BORDERSIZE),
                                    wallColor);

            if (GameMain.endState == 0)
            {
                for (int k = 0; k < trapArray.Length; k++)
                {
                    trapArray[k].Draw();
                }

                if (collectable != null)
                    collectable.Draw();
            }
            for (int k = 0; k < objectArray.Length; k++)
            {
                objectArray[k].Draw();
            }
        }

        public void addPlayer(Character chara)
        {
            if (chara.CurrentRoom != null && chara.CurrentRoom != this)
            {
                if(this.bathroom && GameMain.playerOne.CurrentRoom != this && GameMain.playerTwo.CurrentRoom != this)
                {
                    foreach(Object obj in this.objectArray)
                    {
                        if(obj is Toilet)
                        {
                            ((Toilet)obj).fade = 0;
                        }
                    }
                }
                chara.CurrentRoom.removePlayer(chara);
            }
            if (!characterArray.Contains(chara))
            {
                characterArray.Add(chara);
                chara.CurrentRoom = this;
                if (((GameMain.listOfPastLevels[GameMain.listOfPastLevels.Count - 1].Count > 2) &&
                    ((GameMain.playerOne.checkAlive() && GameMain.playerTwo.checkAlive() && (GameMain.playerOne.CurrentRoom.bathroom || GameMain.playerOne.CurrentRoom.startRoom) && (GameMain.playerTwo.CurrentRoom.bathroom || GameMain.playerTwo.CurrentRoom.startRoom)) ||
                    (GameMain.playerOne.checkAlive() && !GameMain.playerTwo.checkAlive() && (GameMain.playerOne.CurrentRoom.bathroom || GameMain.playerOne.CurrentRoom.startRoom))||
                    (GameMain.playerTwo.checkAlive() && !GameMain.playerOne.checkAlive() && (GameMain.playerTwo.CurrentRoom.bathroom || GameMain.playerTwo.CurrentRoom.startRoom)))))
                {
                    SoundLoader.Get("PickupCollectible").Play(.4f, 0, 0);
                }
            }
            chara.CurrentRoom = this;
        }

        public void removePlayer(Character chara)
        {
            characterArray.Remove(chara);
        }

        public void addSurroundingRoom(Room room)
        {
            surroundingRooms.Add(room);
        }
    }
}
