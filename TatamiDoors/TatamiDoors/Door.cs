﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace TatamiDoors
{
    enum DoorSide
    {
        right, down, left, up
    };

    public class Door : Entity
    {
        private Color basecolor;
        private DoorSide sideOne;
        private DoorSide sideTwo;
        private Point tileNumberOne;
        private Point tileNumberTwo;
        private Room roomOne;
        private Room roomTwo;

        public Door(Room primaryRoom, Room secondaryRoom, Point tileNumOne, Point tileNumTwo)
        {
            Position = new Vector2((float)(tileNumOne.X + tileNumTwo.X) / 2 + .5f, 
                                   (float)(tileNumOne.Y + tileNumTwo.Y) / 2 + .5f) * Global.TileSize.X;
            roomOne = primaryRoom;
            roomTwo = secondaryRoom;
            tileNumberOne = tileNumOne;
            tileNumberTwo = tileNumTwo;
            generateNewColor();

            DoorSide primarySide;
            if (tileNumberOne.X == tileNumberTwo.X + 1)
                primarySide = DoorSide.left;
            else if (tileNumberOne.X + 1 == tileNumberTwo.X)
                primarySide = DoorSide.right;
            else if (tileNumberOne.Y == tileNumberTwo.Y + 1)
                primarySide = DoorSide.down;
            else if (tileNumberOne.Y + 1 == tileNumberTwo.Y)
                primarySide = DoorSide.up;
            else
                throw new ArgumentException("This door does not make sense.");

            if (primarySide == DoorSide.right || primarySide == DoorSide.left)
            {
                boundingRect.Width = 6;
                boundingRect.Height = 38;
            }
            else
            {
                boundingRect.Width = 38;
                boundingRect.Height = 6;
            }

            sideOne = primarySide;
            sideTwo = (DoorSide)(((int)sideOne + 2) % 4);
        }

        public void generateNewColor()
        {
            basecolor = new Color(Rand.Int(56) + 200,
                                  Rand.Int(56) + 200,
                                  Rand.Int(56) + 200);
        }

        public override void CalculateVisibility()
        {
            if (roomOne.IsVisible || roomTwo.IsVisible)
            {
                IsVisible = true;
                if (!GameMain.visibleDoors.Contains(this))
                {
                    GameMain.visibleDoors.Add(this);
                    GameMain.listOfSeenDoorsEver[GameMain.CurrentLevel - 1].Add(this);
                }
            }
            else
                GameMain.visibleDoors.Remove(this);

            if (!IsVisible)
            {
                animationframe = 0;
                animationincreasing = false;
            }
        }

        public Room getConnectingRoomTo(Room room)
        {
            if (room.Equals(roomOne))
            {
                return roomTwo;
            }
            else
            {
                return roomOne;
            }
        }

        const int ANIMATIONLENGTH = 8;
        public int animationframe;
        int animationcooldown;
        bool animationincreasing;
        void checkplayercollision(Character c)
        {
            if (!c.checkAlive()) return;
            if (sideOne == DoorSide.left || sideOne == DoorSide.right)
            {
                if (boundingRect.Top > c.boundingRect.Top || boundingRect.Bottom < c.boundingRect.Bottom
                   || Math.Abs(c.boundingRect.Center.X - boundingRect.Center.X) > c.boundingRect.Width / 2)
                    return;

                c.CurrentDoor = this;
                c.temporarilyDisableRoomCollision_Horizontal();

                if (c.boundingRect.Center.X > boundingRect.Center.X)
                {
                    if (sideOne == DoorSide.left)
                    {
                        roomOne.addPlayer(c);
                        GameMain.listOfPastLevels[GameMain.listOfPastLevels.Count - 1].Add(roomOne);
                    }
                    else
                    {
                        roomTwo.addPlayer(c);
                        GameMain.listOfPastLevels[GameMain.listOfPastLevels.Count - 1].Add(roomTwo);
                    }
                }
                else
                {
                    if (sideOne == DoorSide.left)
                    {
                        roomTwo.addPlayer(c);
                        GameMain.listOfPastLevels[GameMain.listOfPastLevels.Count - 1].Add(roomTwo);
                    }
                    else
                    {
                        roomOne.addPlayer(c);
                        GameMain.listOfPastLevels[GameMain.listOfPastLevels.Count - 1].Add(roomOne);
                    }
                }
            }
            else if (sideOne == DoorSide.up || sideOne == DoorSide.down)
            {
                if (boundingRect.Left > c.boundingRect.Left || boundingRect.Right < c.boundingRect.Right
                   || Math.Abs(c.boundingRect.Center.Y - boundingRect.Center.Y) > c.boundingRect.Height / 2)
                    return;

                c.CurrentDoor = this;
                c.temporarilyDisableRoomCollision_Vertical();

                if (c.boundingRect.Center.Y < boundingRect.Center.Y)
                {
                    if (sideOne == DoorSide.up)
                    {
                        roomOne.addPlayer(c);
                        GameMain.listOfPastLevels[GameMain.listOfPastLevels.Count - 1].Add(roomOne);
                    }
                    else
                    {
                        roomTwo.addPlayer(c);
                        GameMain.listOfPastLevels[GameMain.listOfPastLevels.Count - 1].Add(roomTwo);
                    }
                }
                else
                {
                    if (sideOne == DoorSide.up)
                    {
                        roomTwo.addPlayer(c);
                        GameMain.listOfPastLevels[GameMain.listOfPastLevels.Count - 1].Add(roomTwo);
                    }
                    else
                    {
                        roomOne.addPlayer(c);
                        GameMain.listOfPastLevels[GameMain.listOfPastLevels.Count - 1].Add(roomOne);
                    }
                }
            }
            animationcooldown = 30;
            animationincreasing = true;
        }

        public override void Update()
        {
            base.Update();

            checkplayercollision(GameMain.playerOne);
            checkplayercollision(GameMain.playerTwo);

            if (animationincreasing || animationframe != 0)
            {
                if(animationframe == 0)
                    SoundLoader.Get("DoorClose").Play(.4f,0,0);
                if (animationincreasing && animationframe < ANIMATIONLENGTH - 1)
                {
                    if (++animationframe == ANIMATIONLENGTH - 1)
                        animationincreasing = false;
                }
                else if (animationcooldown != 0)
                    animationcooldown--;
                else
                    animationframe--;
            }
        }

        public override void Draw()
        {
            if (animationframe != 0)
            {
                GraphicsDraw.Draw_World("door" + animationframe, 
                                        Position, 
                                        new Vector2(52, 54), 
                                        basecolor, 
                                        (float)Math.PI / 2 * ((int)sideOne));
            }
            GraphicsDraw.Draw_World("Square", Position, new Vector2(6, 26), Color.Black, (float)Math.PI / 2 * ((int)sideOne));
            GraphicsDraw.Draw_World("Square", Position, new Vector2(4, 24), basecolor, (float)Math.PI / 2 * ((int)sideOne));
        }
    }
}
