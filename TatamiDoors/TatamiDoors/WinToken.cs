﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TatamiDoors
{
    public class WinToken : Entity
    {
        int levelExtension;

        float glow = 0;
        float glowAdd = 1.2f;
        Rectangle collisionBox;

        public WinToken(Vector2 pos)
        {
            Position = pos;
            boundingRect = new Rectangle((int)pos.X, (int)pos.Y, 32, 32);
            collisionBox = new Rectangle((int)pos.X + 2, (int)pos.Y + 2, 28, 28);
            levelExtension = 11 - GameMain.CurrentLevel;
        }

        private void checkPlayer(Character chara)
        {
            if (GameMain.endState == 0)
            {
                if (this.collisionBox.Contains(chara.boundingRect.Center))
                {
                    if (chara.checkAlive())
                    {
                        if (levelExtension == 0)
                            GameMain.EndGame(chara.playerNum);
                        else
                            GameMain.WinGame(chara.playerNum);
                        chara.loading = true;
                        chara.invincible = true;
                    }
                }
            }
        }

        public override void Update_Physics()
        {
        }

        public override void Update()
        {
            if (IsVisible)
            {
                if (glow <= 0)
                    glowAdd = 1.2f;
                else if (glow >= 40)
                    glowAdd = -1.2f;
                glow += glowAdd;

                checkPlayer(GameMain.playerOne);
                checkPlayer(GameMain.playerTwo);
            }
        }

        public override void Draw()
        {
            if (GameMain.endState == 0)
            {
                float angleoffset = (float)Global.Cycle * .15f;
                for (int i = 0; i < 5; i++)
                {
                    Color r = new Color(.05f, .05f, .05f, .05f);
                    GraphicsDraw.Draw_World("GlowTriangle", Global.Vector2FromRectCenter(boundingRect), new Vector2(800, 800), r, (float)Math.PI * 2 / 5 * i + angleoffset);
                    GraphicsDraw.Draw_World("GlowTriangle", Global.Vector2FromRectCenter(boundingRect), new Vector2(1200, 600), r, (float)Math.PI * 2 / 5 * (.5f + i) + angleoffset);
                }

                if (GameMain.playerOne.CurrentRoom.boundingRect.Intersects(this.boundingRect) ||
                    GameMain.playerTwo.CurrentRoom.boundingRect.Intersects(this.boundingRect))
                {
                    if (levelExtension == 0)
                        GraphicsDraw.Draw_World("Mochiguma", boundingRect, new Color(255, 255, 255));
                    else
                        GraphicsDraw.Draw_World("end" + levelExtension, boundingRect, new Color(255, 255, 255));
                }
            }
            else
            {
                if (levelExtension == 0)
                    GraphicsDraw.Draw_World("Mochiguma", boundingRect, new Color(255, 255, 255));
                else
                    GraphicsDraw.Draw_World("end" + levelExtension, boundingRect, new Color(255, 255, 255));
            }
        }
    }
}