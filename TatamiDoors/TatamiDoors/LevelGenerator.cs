﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace TatamiDoors
{
    class HazardArea
    {
        public HazardArea(Vector2 pos, float rad) { Position = pos; Radius = rad; }
        public Vector2 Position;
        public float Radius;
    }
    class LevelGenerator
    {
        const int MINROOMS = 0;
        const int MAXEXPANSIONS = 128;
        const int MAXROOMLENGTH = 20;

        private List<HazardArea> hazards;

        List<Rectangle> RoomRects;
        public List<Room> Rooms;
        public LevelGenerator()
        {
            RoomRects = new List<Rectangle>();
            Rooms = new List<Room>();
            hazards = new List<HazardArea>();
        }

        public TrapSpinningTrap Trap_Spinny(Vector2 pos)
        {
            return new TrapSpinningTrap(pos);
        }

        public TrapFloorSpike Trap_Spiky(Vector2 pos)
        {
            return new TrapFloorSpike(pos);
        }

        const float BLAH = 150;
        public ShootingTrap Trap_Shooter(Vector2 pos, int dir, Room room)
        {
            return new ShootingTrap(pos, dir == 0 ? new Vector2(BLAH, 0) : dir == 1 ? new Vector2(0, BLAH) : dir == 2 ? new Vector2(-BLAH, 0) : new Vector2(0, -BLAH),
                                    100,
                                    new Vector4(room.boundingRect.Left, room.boundingRect.Top, room.boundingRect.Right, room.boundingRect.Bottom),
                                    (float)Math.PI / 2 * dir);
        }

        bool CheckRoomFree(Rectangle rect)
        {
            foreach (Rectangle p in RoomRects)
            {
                if (rect.Intersects(p))
                {
                    return false;
                }
            }
            return true;
        }

        bool CheckPointFree(Point p)
        {
            foreach (Rectangle rect in RoomRects)
                if (rect.Contains(p))
                    return false;
            return true;
        }

        void FinalizeRoom(Rectangle rect, Room hostroom, Point? hostdoorpos, Point? thisdoorpos, bool startRoom = false, bool bathroom = false)
        {
            int tilesize = (int)Global.TileSize.X;
            Room r = new Room(RoomRects.Count,
                              rect.Left * tilesize,
                              rect.Top * tilesize,
                              rect.Width,
                              rect.Height,
                              startRoom,
                              bathroom);
            r.Register();

            if (RoomRects.Count == 0)
                StartRoom = r;

            RoomRects.Add(rect);
            Rooms.Add(r);

            if (hostdoorpos.HasValue)
            {
                Door d = new Door(hostroom, r, hostdoorpos.Value, thisdoorpos.Value);
                d.Register();
                hostroom.addSurroundingRoom(r);
                r.addSurroundingRoom(hostroom);
                hostroom.doors.Add(d);
                r.doors.Add(d);
            }
        }

        bool checkhazardfree(List<HazardArea> h, Vector2 pos, float radiusincrease = 0, bool shootingPlacement = false)
        {
            Vector2 v;

            foreach (HazardArea hazard in h)
            {
                if (shootingPlacement)
                {
                    if ((pos.X >= hazard.Position.X - 16 &&
                         pos.X <= hazard.Position.X + 16) ||
                        (pos.Y >= hazard.Position.Y - 16 &&
                         pos.Y <= hazard.Position.Y + 16))
                        return false;
                }

                v = new Vector2(Math.Abs(hazard.Position.X - pos.X),
                                Math.Abs(hazard.Position.Y - pos.Y));
                if (v.X <= hazard.Radius + radiusincrease &&
                   v.Y <= hazard.Radius + radiusincrease)
                    return false;
            }
            return true;
        }
        void FillRoomWithTraps(Rectangle rect, Room room)
        {
            if (room.bathroom) return; // Avoid bathrooms
            List<Trap> traps = new List<Trap>();
            List<HazardArea> trapHazards = new List<HazardArea>();

            if (rect.Height == 1 || rect.Width == 1) // Don't put traps in a room with width or height of 1
                return;

            bool smallRoom = true;

            if (rect.Height > 4 && rect.Width > 4)
                smallRoom = false;

            int posx = room.boundingRect.Left / 32;
            int posy = room.boundingRect.Top / 32;
            int sizx = room.boundingRect.Width / 32;
            int sizy = room.boundingRect.Height / 32;

            Vector2 pos;

            for (int i = 0; i < 64; i++)
            {
                int type = Rand.Int(4);
                if ((type == 0 || type == 1) && !smallRoom) // Spinning trap
                {
                    pos = new Vector2(posx + Rand.Int(sizx - 1), posy + Rand.Int(sizy - 1)) * 32;
                    pos.X += 32;
                    pos.Y += 32;
                    if (checkhazardfree(hazards, pos, 32) && checkhazardfree(trapHazards, pos, 32))
                    {
                        trapHazards.Add(new HazardArea(pos, 56));
                        pos.X -= 32;
                        pos.Y -= 32;
                        traps.Add(Trap_Spinny(pos));
                    }
                }

                else if (type == 2) // Spike trap
                {
                    pos = new Vector2(posx + Rand.Int(sizx), posy + Rand.Int(sizy)) * 32;
                    pos.X += 16;
                    pos.Y += 16;
                    if (checkhazardfree(hazards, pos) && checkhazardfree(trapHazards, pos))
                    {
                        trapHazards.Add(new HazardArea(pos, 40));
                        pos.X -= 16;
                        pos.Y -= 16;
                        traps.Add(Trap_Spiky(pos));
                    }
                }

                else if (type == 3 && !smallRoom) // Shooting trap
                {
                    int trapx = posx + Rand.Int(sizx);
                    int trapy = posy + Rand.Int(sizy);
                    pos = new Vector2(trapx, trapy) * 32;
                    pos.X += 16;
                    pos.Y += 16;

                    if (checkhazardfree(hazards, pos, 0, true) && checkhazardfree(trapHazards, pos))
                    {
                        trapHazards.Add(new HazardArea(pos, 40));
                        pos.X -= 16;
                        pos.Y -= 16;
                        int a = trapx > posx + sizx / 2 ? 2 : 0;
                        int b = trapy > posy + sizy / 2 ? 3 : 1;
                        traps.Add(Trap_Shooter(pos, Rand.Chance(.5f) ? a : b, room));
                    }
                }
            }

            room.SetTraps(traps.ToArray());
        }

        void FillRoomWithCollectable(Room room)
        {
            if (room.bathroom) return; // Avoid bathrooms

            if (Rand.Chance(.5f))
            {
                int posx = room.boundingRect.Left / 32;
                int posy = room.boundingRect.Top / 32;
                int sizx = room.boundingRect.Width / 32;
                int sizy = room.boundingRect.Height / 32;

                Vector2 pos = new Vector2(posx + Rand.Int(sizx), posy + Rand.Int(sizy)) * 32;
                pos.X += 16;
                pos.Y += 16;

                if (!checkhazardfree(hazards, pos, -32))
                    return;

                pos.X -= 16;
                pos.Y -= 16;
                Rectangle collisionBox = new Rectangle((int)pos.X, (int)pos.Y, 32, 32);
                for (int t = 0; t < room.trapArray.Length; t++)
                {
                    if (room.trapArray[t].boundingRect.Contains(collisionBox))
                        return;
                }

                //Generate and add collectable here.
                room.SetCollectable(new Collectable((int)pos.X,
                                                    (int)pos.Y,
                                                    room.boundingRect));
            }
        }

        void FillRoomWithObjects(Rectangle rect, Room room)
        {
            List<Object> objects = new List<Object>();
            float rotation = (float)Math.PI / 2 * Rand.Int(4); // 0, 1, 2, 3 * 90 degrees
            objects.Add(new Toilet(rect.Center.X * 32, rect.Center.Y * 32, rotation, room));

            rotation = (float)Math.PI / 2 * Rand.Int(2); // 0, 1 * 90 degrees
            int offsetX = 0;
            int offsetY = 0;
            int r;

            // Does the random placement for the bathtub within the room
            if (rotation == 0)
            {
                r = Rand.Int(2);
                if (r == 0)
                    offsetX = -32;
                else
                    offsetX = 32;

                r = Rand.Int(2);
                if (r == 0)
                    offsetY = -32;
            }
            else
            {
                offsetX += 16;
                offsetY -= 16;

                r = Rand.Int(2);
                if (r == 0)
                    offsetX -= 32;

                r = Rand.Int(2);
                if (r == 0)
                    offsetY -= 32;
                else
                    offsetY += 32;
            }

            objects.Add(new Bathtub(rect.Center.X * 32 + offsetX, rect.Center.Y * 32 + offsetY, rotation));

            room.SetObjects(objects.ToArray());
        }

        void CreateBathroom()
        {
            Room hostroom;
            Rectangle hostroomrect;
            Point startpoint, hostpoint;

            for (; ; )
            {
                int i = Rand.Int(RoomRects.Count);
                hostroom = Rooms[i];
                if ((hostroom.startRoom || hostroom.bathroom) && GameMain.CurrentLevel != 1) continue; // At the very least, two bathrooms should not be right next to each other
                hostroomrect = RoomRects[i];
                startpoint = new Point();
                hostpoint = new Point();
                if (Rand.Chance(.5f))
                {
                    startpoint.X = Rand.Int(hostroomrect.Left, hostroomrect.Right);
                    if (Rand.Chance(.5f))
                    {
                        hostpoint = new Point(startpoint.X, hostroomrect.Top);
                        startpoint.Y = hostpoint.Y - 1;
                    }
                    else
                    {
                        hostpoint = new Point(startpoint.X, hostroomrect.Bottom - 1);
                        startpoint.Y = hostpoint.Y + 1;
                    }
                }
                else
                {
                    startpoint.Y = Rand.Int(hostroomrect.Top, hostroomrect.Bottom);
                    if (Rand.Chance(.5f))
                    {
                        hostpoint = new Point(hostroomrect.Left, startpoint.Y);
                        startpoint.X = hostpoint.X - 1;
                    }
                    else
                    {
                        hostpoint = new Point(hostroomrect.Right - 1, startpoint.Y);
                        startpoint.X = hostpoint.X + 1;
                    }
                }

                Rectangle newroomrect;
                int r = Rand.Int(3) + 1; // For deciding which side to use for the new bathroom

                // Cases for top, bottom, left, and right side bathrooms
                if (startpoint.X > hostpoint.X)
                {
                    if (r == 1)
                        newroomrect = new Rectangle(startpoint.X, startpoint.Y, 3, 3);
                    else if (r == 2)
                        newroomrect = new Rectangle(startpoint.X, startpoint.Y - 1, 3, 3);
                    else
                        newroomrect = new Rectangle(startpoint.X, startpoint.Y - 2, 3, 3);
                }
                else if (startpoint.X < hostpoint.X)
                {
                    if (r == 1)
                        newroomrect = new Rectangle(startpoint.X - 2, startpoint.Y, 3, 3);
                    else if (r == 2)
                        newroomrect = new Rectangle(startpoint.X - 2, startpoint.Y - 1, 3, 3);
                    else
                        newroomrect = new Rectangle(startpoint.X - 2, startpoint.Y - 2, 3, 3);

                }
                else if (startpoint.Y > hostpoint.Y)
                {
                    if (r == 1)
                        newroomrect = new Rectangle(startpoint.X, startpoint.Y, 3, 3);
                    else if (r == 2)
                        newroomrect = new Rectangle(startpoint.X - 1, startpoint.Y, 3, 3);
                    else
                        newroomrect = new Rectangle(startpoint.X - 2, startpoint.Y, 3, 3);
                }
                else // (startpoint.Y < hostpoint.Y)
                {
                    if (r == 1)
                        newroomrect = new Rectangle(startpoint.X, startpoint.Y - 2, 3, 3);
                    else if (r == 2)
                        newroomrect = new Rectangle(startpoint.X - 1, startpoint.Y - 2, 3, 3);
                    else
                        newroomrect = new Rectangle(startpoint.X - 2, startpoint.Y - 2, 3, 3);
                }

                if (CheckRoomFree(newroomrect))
                {
                    FinalizeRoom(newroomrect, hostroom, hostpoint, startpoint, false, true);
                    break;
                }
            }
        }

        void CreateRandomRoom()
        {
            Room hostroom;
            Rectangle hostroomrect;
            Point startpoint, hostpoint;
            for (; ; )
            {
                int i = Rand.Int(RoomRects.Count);
                hostroom = Rooms[i];
                hostroomrect = RoomRects[i];
                startpoint = new Point();
                hostpoint = new Point();
                if (Rand.Chance(.5f))
                {
                    startpoint.X = Rand.Int(hostroomrect.Left, hostroomrect.Right);
                    if (Rand.Chance(.5f))
                    {
                        hostpoint = new Point(startpoint.X, hostroomrect.Top);
                        startpoint.Y = hostpoint.Y - 1;
                    }
                    else
                    {
                        hostpoint = new Point(startpoint.X, hostroomrect.Bottom - 1);
                        startpoint.Y = hostpoint.Y + 1;
                    }
                }
                else
                {
                    startpoint.Y = Rand.Int(hostroomrect.Top, hostroomrect.Bottom);
                    if (Rand.Chance(.5f))
                    {
                        hostpoint = new Point(hostroomrect.Left, startpoint.Y);
                        startpoint.X = hostpoint.X - 1;
                    }
                    else
                    {
                        hostpoint = new Point(hostroomrect.Right - 1, startpoint.Y);
                        startpoint.X = hostpoint.X + 1;
                    }
                }
                if (CheckPointFree(startpoint)) break;
            }

            Rectangle newroomrect = new Rectangle(startpoint.X, startpoint.Y, 1, 1);
            Rectangle growrect;
            for (int i = 0; i < Rand.Int(1, MAXEXPANSIONS); i++)
            {
                if (newroomrect.Width == MAXROOMLENGTH || newroomrect.Height == MAXROOMLENGTH) break;
                if (Rand.Chance(.5f))
                {
                    if (Rand.Chance(.5f))
                        growrect = new Rectangle(newroomrect.Left, newroomrect.Top - 1, newroomrect.Width, newroomrect.Height + 1);
                    else
                        growrect = new Rectangle(newroomrect.Left, newroomrect.Top, newroomrect.Width, newroomrect.Height + 1);
                }
                else
                {
                    if (Rand.Chance(.5f))
                        growrect = new Rectangle(newroomrect.Left - 1, newroomrect.Top, newroomrect.Width + 1, newroomrect.Height);
                    else
                        growrect = new Rectangle(newroomrect.Left, newroomrect.Top, newroomrect.Width + 1, newroomrect.Height);
                }
                if (CheckRoomFree(growrect))
                    newroomrect = growrect;
            }
            FinalizeRoom(newroomrect, hostroom, hostpoint, startpoint);
        }

        Room StartRoom;
        public List<Room> Generate()
        {
            Rectangle startroomrect = new Rectangle(0, 0, 3, 3);
            FinalizeRoom(startroomrect, null, null, null, true, true);

            FillRoomWithObjects(RoomRects[0], Rooms[0]);

            // Rooms multiply by two every level
            int maxRooms = (int)(MINROOMS + (Math.Pow(1.618, GameMain.CurrentLevel - 1) * 5));
            Point farthest = new Point();
            float distsq = -1;
            int roomNumber = -1;
            for (int i = 1; i <= maxRooms; i++)
            {
                if ((i == maxRooms) || // One bathroom per level
                    ((i + 12) % 24 == 0)) // Every 24 rooms will have one bathroom, created roughly during the middle of that set
                {
                    CreateBathroom();
                    FillRoomWithObjects(RoomRects[i], Rooms[i]);
                    Point pos = RoomRects[i].Center;
                    float d = pos.X * pos.X + pos.Y * pos.Y;
                    if (d > distsq)
                    {
                        distsq = d;
                        farthest = pos;
                        roomNumber = i;
                    }
                }
                else
                    CreateRandomRoom();
            }

            Rooms[roomNumber].endRoom = true;
            WinToken wt = new WinToken(new Vector2(farthest.X, farthest.Y) * Global.TileSize);
            wt.Register();

            Rooms[roomNumber].winToken = wt;

            for (int i = 0; i < Rooms.Count; i++)
            {
                hazards = new List<HazardArea>();
                foreach (Door d in Rooms[i].doors)
                    hazards.Add(new HazardArea(d.Position, 56));

                FillRoomWithTraps(RoomRects[i], Rooms[i]);
                FillRoomWithCollectable(Rooms[i]);
            }

            return Rooms;
        }

        public void StartLevel()
        {
            GameMain.playerOne = new Character(1, 16, 48);
            GameMain.playerTwo = new Character(2, 80, 48);
            StartRoom.addPlayer(GameMain.playerOne);
            StartRoom.addPlayer(GameMain.playerTwo);
            StartRoom.startRoom = true;
            GameMain.listOfPastLevels[GameMain.listOfPastLevels.Count - 1].Add(StartRoom);
            GameMain.playerOne.Register();
            GameMain.playerTwo.Register();
        }
    }
}
