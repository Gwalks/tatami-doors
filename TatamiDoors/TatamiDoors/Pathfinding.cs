﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TatamiDoors
{
    struct searchNode
    {
        public Room room;
        public float hScore;
        public float gScore;
        public float fScore;
        public object parent;
    }

    public class Pathfinding
    {
        public List<Vector2> path;
        public float distance;
        private List<Room> level;
        private Room start;
        private Room end;

        public Pathfinding(List<Room> lvl, Room startRoom, Room endRoom)
        {
            level = lvl;
            start = startRoom;
            end = endRoom;
            path = new List<Vector2>();
            distance = 0;
        }

        public void setNewLevel(List<Room> lvl, Room startRoom, Room endRoom)
        {
            level = lvl;
            start = startRoom;
            end = endRoom;
            distance = 0;
        }

        //This returns a value that represents how good the room is to get to the
        //end.  The better this is the better the algorithum.
        private float heuristic(Room testRoom)
        {
            //Right now just return the distance between the two rooms.
            return Vector2.Distance(new Vector2(end.boundingRect.Center.X, end.boundingRect.Center.Y), new Vector2(testRoom.boundingRect.Center.X, testRoom.boundingRect.Center.Y));
        }

        public void findPath()
        {
            path.Clear();
            //List of rooms being considered.
            List<searchNode> openList = new List<searchNode>();
            //List of rooms that we do not need to consider anymore.
            List<searchNode> closedList = new List<searchNode>();

            //The start room is done.
            searchNode startNode = new searchNode();
            startNode.room = start;
            startNode.gScore = 0;
            startNode.hScore = 0;
            startNode.fScore = 0;
            closedList.Add(startNode);

            //Add all surrounding rooms to the open list.
            for (int i = 0; i < start.surroundingRooms.Count; i++)
            {
                searchNode newNode;
                newNode.room = start.surroundingRooms[i];
                newNode.gScore = Vector2.Distance(new Vector2(start.boundingRect.Center.X, start.boundingRect.Center.Y), new Vector2(start.surroundingRooms[i].boundingRect.Center.X, start.surroundingRooms[i].boundingRect.Center.Y));
                newNode.hScore = heuristic(start.surroundingRooms[i]);
                newNode.fScore = newNode.gScore + newNode.hScore;
                newNode.parent = startNode;
                openList.Add(newNode);
            }

            //The meat of the algorithum
            while (openList.Count > 0)
            {
                searchNode lowNode;
                float minDist = 9999;
                int lowNodeIndex = -1;
                //Find the lowest cost node and begin calculation.
                for (int i = 0; i < openList.Count; i++)
                {
                    if (openList[i].fScore < minDist)
                    {
                        minDist = openList[i].fScore;
                        lowNode = openList[i];
                        lowNodeIndex = i;
                    }
                }
                lowNode = openList[lowNodeIndex];

                openList.RemoveAt(lowNodeIndex);
                closedList.Add(lowNode);

                //Check to see if we are at the end.
                if (lowNode.room.boundingRect.Center.Equals(end.boundingRect.Center))
                {
                    //keep a list to draw later.
                    List<Door> totalDoorsOnPath = new List<Door>();

                    //Go backward through the parents of the nodes to find the path.
                    searchNode backNode = lowNode;

                    totalDoorsOnPath.AddRange(backNode.room.doors);

                    backNode.room.IsVisible = true;
                    EntityService.Register(backNode.room);

                    //Save the end room to draw ontop of everything.
                    Room endingRoom = backNode.room;

                    for (int t = 0; t < backNode.room.trapArray.Length; t++)
                    {
                        backNode.room.trapArray[t].IsVisible = true;
                        EntityService.Register(backNode.room.trapArray[t]);
                    }

                    path.Clear();
                    path.Add(new Vector2(backNode.room.boundingRect.Center.X, backNode.room.boundingRect.Center.Y));
                    while ((backNode.fScore != 0 || backNode.gScore != 0 || backNode.hScore != 0) && backNode.parent != null)
                    {
                        backNode = (searchNode)backNode.parent;
                        path.Add(new Vector2(backNode.room.boundingRect.Center.X, backNode.room.boundingRect.Center.Y));
                        distance += Vector2.Distance(path[path.Count - 1], path[path.Count - 2]);

                        backNode.room.IsVisible = true;
                        EntityService.Register(backNode.room);

                        for (int t = 0; t < backNode.room.trapArray.Length; t++)
                        {
                            backNode.room.trapArray[t].IsVisible = true;
                            EntityService.Register(backNode.room.trapArray[t]);
                        }

                        totalDoorsOnPath.AddRange(backNode.room.doors);
                    }

                    for (int d = 0; d < totalDoorsOnPath.Count; d++)
                    {
                        totalDoorsOnPath[d].IsVisible = true;
                        totalDoorsOnPath[d].animationframe = 0;
                        EntityService.Register(totalDoorsOnPath[d]);
                    }

                    endingRoom.winToken.IsVisible = true;
                    EntityService.Register(endingRoom.winToken);

                    return;
                }

                //If this room was a room in the level.
                //Remeber that we only saved parts of the level the player went to to reduce the load.
                Boolean playerVisited = false;
                for (int i = 0; i < level.Count; i++)
                {
                    if (level[i].boundingRect.Center.X == lowNode.room.boundingRect.Center.X && level[i].boundingRect.Center.Y == lowNode.room.boundingRect.Center.Y)
                    {
                        playerVisited = true;
                    }
                }

                if (playerVisited)
                {
                    //Check the neighbours.
                    float currentNextNodeMinDist = float.MaxValue;
                    for (int p = 0; p < lowNode.room.surroundingRooms.Count; p++)
                    {
                        //Check to see if we searched this room already.
                        //If we did we ignore the room.
                        Boolean alreadSearched = false;
                        for (int c = 0; c < closedList.Count; c++)
                        {
                            if (closedList[c].room.boundingRect.Center.Equals(lowNode.room.surroundingRooms[p].boundingRect.Center))
                            {
                                alreadSearched = true;
                            }
                        }

                        if (!alreadSearched)
                        {
                            //If this is already in the openlist then update the values.
                            Boolean alreadyCandidate = false;
                            int candidateIndex = -1;
                            for (int o = 0; o < openList.Count; o++)
                            {
                                if (openList[o].room.boundingRect.Center.Equals(lowNode.room.surroundingRooms[p].boundingRect.Center))
                                {
                                    alreadyCandidate = true;
                                    candidateIndex = o;
                                }
                            }

                            //New node.
                            if (!alreadyCandidate)
                            {
                                searchNode newNode = new searchNode();
                                newNode.room = lowNode.room.surroundingRooms[p];
                                newNode.gScore = lowNode.gScore + Vector2.Distance(new Vector2(newNode.room.boundingRect.Center.X, newNode.room.boundingRect.Center.Y), new Vector2(lowNode.room.boundingRect.Center.X, lowNode.room.boundingRect.Center.Y));
                                newNode.hScore = heuristic(newNode.room);
                                newNode.fScore = newNode.gScore + newNode.hScore;
                                newNode.parent = lowNode;
                                currentNextNodeMinDist = newNode.fScore;
                                openList.Add(newNode);
                            }
                            //Update old note.
                            else
                            {
                                searchNode updateNode = openList[candidateIndex];
                                updateNode.gScore = lowNode.gScore + Vector2.Distance(new Vector2(openList[candidateIndex].room.boundingRect.Center.X, openList[candidateIndex].room.boundingRect.Center.Y), new Vector2(lowNode.room.boundingRect.Center.X, lowNode.room.boundingRect.Center.Y));
                                updateNode.hScore = heuristic(openList[candidateIndex].room);
                                updateNode.fScore = openList[candidateIndex].gScore + openList[candidateIndex].hScore;
                                updateNode.parent = lowNode;
                                openList.RemoveAt(candidateIndex);
                                openList.Add(updateNode);
                            }
                        }
                    }
                }
            }
        }
    }
}