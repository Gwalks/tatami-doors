﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;

namespace TatamiDoors{
    public static class SoundLoader{
        public static ContentManager ContentManager;
        static Dictionary<string,SoundEffect> ContentList = new Dictionary<string,SoundEffect>();

        public static void Initialize(ContentManager manager){
            ContentManager = manager;
        }

        //Clears all assets from memory (they will be reloaded when used again)
        public static void Clear(){
            foreach(SoundEffect blah in ContentList.Values)
                blah.Dispose();
            ContentList.Clear();
        }

        //Gets a sound form filepath
        public static SoundEffect Get(string name){
            if(!ContentList.ContainsKey(name))
                ContentList.Add(name,ContentManager.Load<SoundEffect>(name));
            return ContentList[name];
        }
    }
}
